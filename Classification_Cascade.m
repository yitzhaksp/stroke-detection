addpath('../useful_functions');
addpath(genpath('.'));

%the cascade algorithm requires that Classifiers are already trained
PlotConf=0;
PlotStat=1;

trh=0.2; % treshold for confidence
% all samples with thr<=Conf<=1-trh
%will be propagated to the next classifier

%params for knearneig
dummy=0;
nNeig=4; % num of neighbours
opts.weight=2; % weights: 1-equal, 2-gaussian dist
opts.conf=2; %confidence type 1-correct, 2-common 
opts.gamma=1.0; % width of gaussian
FeatTest=Feat(:,IndTest);

%Conf1=Clsf1(FeatTest,FeatTrain,dummy,ClTrain,nNeig,opts);
%Conf1=Clsf2(svmStruct, FeatTest');
Conf1=Clsf3( nnet1,FeatTest,DummyClass );
[ind1,Prd1,dummy,stat1,dummy1]=comp_misc1(n_test,Conf1',[],Cl(IndTest), ones(1,n_test) ,trh);

Conf2=zeros(1,n_test);
Conf2(~ind1)=Clsf1( FeatTest(:,(~ind1)) , Feat(:,IndTrain),dummy,Cl(IndTrain),nNeig,opts);
%Conf2(~ind1)=Clsf2(svmStruct, (FeatTest(:,(~ind1)))');
Conf2(~ind1)=Clsf3( nnet1 , FeatTest(:,(~ind1)),DummyClass );
Conf2(ind1)=-100; %meaningless
[indincr1,PrdIncr1,Prd2,statincr1,stat2]=comp_misc1(n_test,(Conf2(~ind1))',Prd1,Cl(IndTest),(~ind1),trh);
%figure;hist(Conf2);
ind2=logical(ind1+indincr1);

trh1=0.6; % trh>0.5 means that in fact there is no treshhold
Conf3=zeros(1,n_test);
Conf3(~ind2)=Clsf1(FeatTest(:,(~ind2)),Feat(:,IndTrain),dummy,Cl(IndTrain),nNeig,opts);
%Conf3(~ind2)=Clsf2(svmStruct, (FeatTest(:,(~ind2)))');
%Conf3(~ind2)=Clsf3( nnet1 , FeatTest(:,(~ind2)),DummyClass );
Conf3(ind2)=-100; %meaningless
[indincr2,PrdIncr2,Prd3,statincr2,stat3]=comp_misc1(n_test,(Conf3(~ind2))',Prd2,Cl(IndTest),(~ind2),trh1);
ind3=logical(ind2+indincr2);

Conf=zeros(size(ClTest));
Conf(ind1)=Conf1(ind1);
Conf( indincr1 )=Conf2(indincr1);
Conf( indincr2 )=Conf3(indincr2);
%Conf( indincr1 )=Conf2(trfind_inv(indincr1,(~ind1)));
%Conf( indincr2 )=Conf2(trfind_inv(indincr2,(~ind2)));
PrdTest=zeros(size(ClTest));
PrdTest(Conf>=0.5)=1;
PrdTest(Conf<0.5)=2;
IndClsf=zeros(1,n_test);
IndClsf(ind1)=1;
IndClsf(indincr1)=2;
IndClsf(indincr2)=3;
cnf=comp_stat_cnf(ClTest,Conf);

%compute confidence of correct classification
nNeig=5;
opts.weight=2; % weights: 1-equal, 2-gaussian dist
opts.conf=1; %confidence type 1-correct, 2-positive
opts.gamma=1.0; % width of gaussian
ConfC=ClassifyKNearNeig(FeatTest,FeatTest,PrdTest,ClTest,nNeig,opts);

%compute statistics
h=.1;
trh=[0:h:1];
stat=comp_stat1(PrdTest,ClTest,ConfC,trh);
if (PlotStat)
    subplot(3,1,1), plot(trh,stat.acr,'*'); title('acr');grid on;
    subplot(3,1,2), plot(trh,stat.PPV,'*'); title('PPV'); grid on;
    subplot(3,1,3), plot(trh,stat.NPV,'*'); title('NPV');grid on;
end
if (PlotConf)
    subplot(2,1,1), hist(cnf.ConfP);
    subplot(2,1,2), hist(cnf.ConfN);
end