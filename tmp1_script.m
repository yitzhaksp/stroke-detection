%{
given n discrete functions y(j,:), 1<=j<=n
corresponding to possibly different different x-value-sets x(i,:)
computes the sum of these reltive to a uniform scale of width h
%}
addpath('../useful_functions');
n=2;
X=[X1';X2'];
Y=[Y1';Y2'];
h=.01;
%all the Ys live on one scale
[X_uni,Y_uni]=multscale_to_onescale(X,Y,h);
Y_mean=mean(Y_uni);
figure;plot(X_uni,Y_uni);
hold on
plot(X_uni,Y_mean);