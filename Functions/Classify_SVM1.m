% Classification using MatLab's SVM algorithm
function [Prd,dist,svmStruct]=Classify_SVM1(Feat,FeatTrain,ClTrain,opts,varargin)
nVarargs = length(varargin);
n_train=length(ClTrain);
if (nVarargs>=1)
    params=varargin{1};
end
switch opts.ParamsFlag
    case 1
        %% optimize SVM- and Kernel params via CV
        c = cvpartition(n_train,'kfold',10);
        minfn = @(z)crossval('mcr',FeatTrain',ClTrain,'Predfun', ...
            @(xtrain,ytrain,xtest)crossfun(xtrain,ytrain,...
            xtest,exp(z(1)),exp(z(2))),'partition',c);
        searchopts = optimset('TolX',5e-4,'TolFun',5e-4);
        [searchmin fval] = fminsearch(minfn,randn(2,1),searchopts);
        z = exp(searchmin);
        C=z(2);
        sigma=z(1);   
    case 2    
        load(opts.ParamsPath);
    case 3
        C=params.C;
        sigma=params.sigma;
end
    %% train SVM
    switch opts.Kernel
        case 'rbf'
            svmStruct = svmtrain(FeatTrain',ClTrain,'Kernel_Function','rbf',...
                'rbf_sigma',sigma,'boxconstraint',C,'showplot',true);
        case 'poly'
            svmStruct = svmtrain(Feat(:,IndTrain)',Cl(IndTrain),'Kernel_Function','polynomial','polyorder',3,'showplot',true);
    end
    %% Classify test data
    [Prd,dist] = svmclassify1(svmStruct,Feat','showplot',true);
    %% Classification on cases
    if (opts.Cases)
        %code pieces: 3
    end


