%{
duplicate positive features (label=1) and corresponding classes

inputs:
Feat,Cl: features and corresponding classes

outputs:
FeatDup,ClDup: duplicated features and classes
%}
function [FeatDup,ClDup]=duplicate_positive_samples(Feat,Cl,ndup)
n=size(Feat,2); %num of features
FeatDup=[];
ClDup=[];
for i=1:n
    if (Cl(i)==1)
        for j=1:ndup
            FeatDup=[FeatDup,Feat(:,i)];
            ClDup=[ClDup,1];
        end
    end
end
FeatDup=[Feat,FeatDup];
ClDup=[Cl;ClDup'];
ntot=length(ClDup);
ind=randperm(ntot);
FeatDup=FeatDup(:,ind);
ClDup=ClDup(ind);


