%{
k nearest neighbour classification:
actually this function contains two different algorithms:

the case opts.conf=1: it computes the probability of
correct classification. In this case for the input variables we should have
the following. Divide the Testset IndTest in two sets Indtest1 and
Indtest2.  Let (FeatLb,Prd,Cl) be the (samples,prediction,groundtruth) of an 
arbitrary classifier on IndTest1. Let Feat be the sample on Indtest2, then
for every sample in Feat the alogrithm will compute the probability (call it ConfCorr), that
the classifier used for Indtest1 will make a correct prediction. 
If we want to check that in fact samples which were correctly predicted
have I high ConfCorr we can allow Indtest=Indtest1=Indtest2.

the case opts.conf=2: In this case the algorithm computes the confidence
for every sample of Feat based on the samples from FeatLb

input variables:
FeatLb-Labeled Featurevecs 
Cl- Classes of FeatLb
Feat-samples to be classified
     Feat:(nFeat x nSamples)
Prd: predictions of some classifier for FeatLb
     this variable is only required if opts.conf=1
     if not, put some dummy variable
opts.weight: weights: 1-equal, 2-gaussian dist
opts.conf: confidence type 1-correct, 2-positive 
opts.gamma: width of gaussian
%}
function Conf=ClassifyKNearNeig(Feat,FeatLb,Prd,Cl,nNeig,opts)
N=size(Feat,2);
%the maximal distance shall get the weight radbas(xmax)
xmax=2;
Neig=knnsearch(FeatLb',Feat','k',nNeig);
if size(Cl,2)==1
    Cl=Cl';
end
if size(Prd,2)==1
    Prd=Prd';
end
Cl(Cl==0)=2; % for ind2vec
W=ind2vec(Cl);
Cl(Cl==2)=0;
if (opts.conf==1)
    Corr=(Prd==Cl); 
end
wght=ones(nNeig,1);
scl=1/sum(wght);
for i=1:N
    if (opts.weight==2)
        dists=dist( (FeatLb(:,Neig(i,:)))' , Feat(:,i) );
        % we want the scaling factor gamma to be such that the
        % most distant neighbour will have the weight radbas(xmax)
        gamma=xmax/max(dists);
        wght=radbas(gamma*dists);
        scl=1/sum(wght);
    end
 
    switch opts.conf
        case 1
            Conf(i)=scl*dot( double(Corr(Neig(i,:))) , wght );
        case 2
            Conf(:,i)=scl*(W(:,Neig(i,:))*wght);     
    end
end