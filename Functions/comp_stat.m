%{
compute statistics
the various statistics can be found in:
https://en.wikipedia.org/wiki/Receiver_operating_characteristic

inputs:
Prd: predictions of classifier
Cl: groundtruth on the same samples as Prd
varargin{1}=ClMain for binary classification in
            case of multiple classes

%}
function stat=comp_stat(Prd,Cl,varargin)
n=length(Prd);
trh_FP_Flag=0;
Cl1=Cl;
Prd1=Prd;
for i=1:length(varargin)
    switch varargin{i}.name
        case 'main_class'
            ClMain=varargin{i}.value;
            Cl1=(Cl==ClMain);
            Prd1=(Prd==ClMain);
        case 'trh_FP'
            trh_FP_Flag=1;
            trh_FP=varargin{i}.value;
            scores=varargin{i}.scores;
    end
end

stat.n=length(Cl);
stat.acr=sum(Prd1==Cl1)/n;
tmp1=Cl1+Prd1;
tmp2=sum(tmp1==2); % #(true_positive (TP))
tmp3=sum(Cl1==1); % positive
stat.pos=tmp3/n;
stat.TPR=tmp2/tmp3; % true positive rate
tmp4=sum(Prd1==1); % # TP+FP
tmp5=sum(tmp1==0); % #(true_negative (TN))
tmp6=sum(Cl1==0); % negative
stat.TNR=tmp5/tmp6;
stat.PPV=tmp2/tmp4; % positive predictive value
c=.7;
stat.acr_w=c*stat.TPR+(1-c)*stat.TNR; %weighted accuracy
if (trh_FP_Flag)
    stat.A_trh_ratio=comp_A_trh_ratio (Cl1,scores,trh_FP);
end

