function [Feat,Cl,ClProb,DataStat,partition_out]=Load_data(partition_in,opts)
[Feat,Cl,ClProb,NCl,IndTrain,IndConf, ...
    IndTest,DataStat]=Load_and_Separate(opts,partition_in);
partition_out.IndTrain=IndTrain;
partition_out.IndConf=IndConf;
partition_out.IndTest=IndTest;
nFeatAct=opts.nFeatAct; % num of active features
load(opts.FeatRankPath);
FeatAct=FeatRank(1:opts.nFeatAct);
Feat=Feat(FeatAct,:);
