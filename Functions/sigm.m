function y=sigm(x)
y=.5*(tanh(2*x)+1);