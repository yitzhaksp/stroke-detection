%{
Compute various quantities in the cascade algorithm

inputs:
n_test: num of test samples
Conf: Confidences output by the classifier in current step
PrdTestLst: Predictions of the steps until now
indsub: indices of samples given to the current classifier
trh: treshhold for confidence

outputs:
indincr: indices of safe samples according to current classifier
PrdTestIncr: Predictions of current classifier on indincr
PrdTest: union of PrdTestLst and PrdTestIncr
statincr: statistics on indincr
stat: statistics on PrdTest
%}

function [indincr,PrdTestIncr,PrdTest,statincr,stat]=comp_misc1(n_test,Conf,PrdTestLst,ClTest,indsub,trh);
tmp1=(Conf<trh);
tmp2=(Conf>(1-trh));
tmp3=tmp1+tmp2;
tmp4=(tmp3>0); %safe inds
indincr=logical(trfind(tmp4,indsub)); %increment: safe inds relative to FeatTest 
indsf=logical((~indsub)+indincr); %safe samples of current and previous steps
PrdTestIncr=ones(size(Conf));
PrdTestIncr(Conf<0.5)=2;
PrdTestIncr=PrdTestIncr(tmp4);% safe samples
statincr=comp_stat(PrdTestIncr,ClTest(indincr));
PrdTest=zeros(n_test,1);
PrdTest(~indsub)=PrdTestLst;
PrdTest(indincr)=PrdTestIncr;
PrdTest=(PrdTest(indsf)); % other entries are meaningless (all 0)
stat=comp_stat(PrdTest,ClTest(indsf));
