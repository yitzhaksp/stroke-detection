function nnet=Train_NN(Feat,Cl,ClProb,NCl,IndTrain,opt)
N=length(Cl);
% the second condition guarantees that dummyclass
% was not yet added in the last program execution
if (opt.DummyClass && (size(ClProb,1)==NCl) )
    %ClProbDummy=zeros(1,N);
    ClProbDummy=0.2*round(rand(1,N));
    ClProb=[ClProbDummy;ClProb];
elseif ((~opt.DummyClass) &&  (size(ClProb,1)>NCl))
    ClProb(1,:)=[];
end
%train networks with random initial weights
    %create neural networks
    %nnet1 = newff(FeatTrain,ClTrain,n_neurons);
    nnet = newff(Feat(:,IndTrain),ClProb(:,IndTrain),opt.n_neurons);
    nnet.trainParam.epochs=50;
    nnet.divideFcn='divideblock';
    nnet.divideParam.trainRatio=0.7;
    nnet.divideParam.valRatio=0.3;
    nnet.divideParam.testRatio=1-nnet.divideParam.trainRatio-nnet.divideParam.valRatio;
    nnet=init(nnet);
    [nnet,tr] = train(nnet,Feat(:,IndTrain),ClProb(:,IndTrain));
    