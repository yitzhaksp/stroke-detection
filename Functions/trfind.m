
%{
transform indices relative to a subvec into
indices relative to vec
inputs:
indbase: indices of subvec 
indsub: indices to be transformed
%}
function indvec=trfind(indsub,indbase)
tmp1=find(indbase>0);
tmp2=tmp1(indsub);
indvec=zeros(1,length(indbase));
indvec(tmp2)=1;

