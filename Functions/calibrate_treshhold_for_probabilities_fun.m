%{
calibrates treshhold for classification.
the classification principle is: score>=trh -> classify as positive
%}

function [trh_opt,acr_w]=calibrate_treshhold_for_probabilities_fun(Lables,scores)
myfun=@(trh) calc_acr_on_probabilities_with_trh(scores,Lables,trh);
h=.01;
trhs=0:h:1; % tresholds
for k=1:length(trhs)
    acr_w(k)=myfun(trhs(k));
end
[acr_w_max,indmax]=max(acr_w);
trh_opt=trhs(indmax);

