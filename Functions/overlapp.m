%{
compute the percentage of the interval int1 that lies in int2
%}
function y=overlapp(int1,int2)
y=0;
eps=1e-8;
tmp1=max(int1(1),int2(1)); %potential xmin of overlap
tmp2=min(int1(2),int2(2)); % potential xmax of overlap 
if (tmp1<tmp2)
    xmin=tmp1;
    xmax=tmp2;
    y=(xmax-xmin)/(int1(2)-int1(1));    
elseif (abs(tmp1-tmp2)<eps) % here we check the case tmp1==tmp2
    if( tmp1>=int2(1) && tmp1<=int2(2))
        y=1;
    end
end