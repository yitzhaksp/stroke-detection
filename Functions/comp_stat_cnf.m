function cnf=comp_stat_cnf(Cl,Conf)
cnf.ConfP=Conf(:,Cl==1);
cnf.ConfN=Conf(:,Cl~=1);
cnf.ConfPMean=mean(cnf.ConfP(1,:));
cnf.ConfNMean=mean(cnf.ConfN(1,:));