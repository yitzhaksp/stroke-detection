function acr=calc_acr_on_probabilities_with_trh(Prob,Cl,trh)
N=length(Cl);
Prd=zeros(size(Prob));
Prd(Prob>=trh)=1;
ind=1:N;
stats=comp_stat(Prd,Cl);
acr=stats.acr_w;

