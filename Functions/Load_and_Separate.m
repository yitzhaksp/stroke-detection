%{
% Load Data and separate into training and testing set

variables:
NCl - number of classes
Feat - Features
Cl- Features Classes
ClProb - Features Classes Probabilities
%}
function [Feat,Cl,ClProb,NCl,IndTrain,IndConf,...
     IndTest,DataStat]=Load_and_Separate(opts,varargin)
load(opts.DataPath)
DataMat=[y,x];
Cl=DataMat(:,1); 
Feat=(DataMat(:,2:end))';
N=size(Feat,2); % number of samples
nFeat=size(Feat,1);
Labels=unique(Cl);
NCl=length(Labels); %number of classes
if (opts.ClBinFlag)
    NCl=2;
    Labels=[0,1];
    ClBin=(Cl==opts.ClMain);
    Cl=double(ClBin);
    ClMainDense=1;
elseif(opts.ClChooseFlag)
    NClChosen=length(ChosenLabels);
    tmp=ismember(Cl,ChosenLabels);
    IndClChosen=find(tmp==1);
    NCl=NClChosen;
    Cl=Cl(IndClChosen); 
    N=length(IndClChosen);
    Feat=Feat(:,IndClChosen);
    Labels=ChosenLabels;   
end

if  (~opts.ClBinFlag && opts.ClDenseFlag)
    ClMainDense=find(Labels==opts.ClMain); %main class in dense version of Cl   
    for i=1:NCl
       Ind=find(Cl==Labels(i));
       ClDense(Ind)=i;
    end
       Cl=ClDense';
end

if (NCl==2)
    ClProb=zeros(NCl,N);
    ClProb(1,logical(Cl))=1;
    ClProb(2,:)= -ClProb(1,:)+1;
    ClProb=sparse(ClProb);
else
    ClProb=ind2vec(Cl');
end

%parameters for separating training data from testing datas
TrainIndManual=0; % bool,how to select training set
n_train = floor(N*opts.train_ratio);
n_Conf = floor(N*opts.conf_ratio);
n_test=N-n_Conf-n_train;

% separate training data from testing data

if(opts.new_perm_FLAG==1 || opts.new_perm_FLAG==2)
    if opts.new_perm_FLAG==1
        Indices=[1:N];
    elseif opts.new_perm_FLAG==2
        Indices=randperm(N); 
    end
    IndTrain=Indices(1:n_train);
    IndConf=sort( Indices((n_train+1):(n_train+n_Conf)) );
    IndTest=sort( Indices((n_train+n_Conf+1):end) );
elseif opts.new_perm_FLAG==3
    partition=varargin{1};
    IndTrain=partition.trainIdx;
    IndConf=partition.confIdx;
    IndTest=partition.testIdx;
    n_train=length(IndTrain);
    n_test=length(IndTest);
    n_Conf=length(IndConf);
end


% duplicate the positive samples in the
% training set in order to crreate a balanced training set
if (opts.DupPos)
    % number of duplicates
    %ind=[IndConf,IndTest];
    FeatTest=Feat(:,IndTest);
    ClTest=Cl(IndTest);
    [FeatTrainDup,ClTrainDup]=duplicate_positive_samples(Feat(:,IndTrain),Cl(IndTrain),opts.ndup);
    [FeatConfDup,ClConfDup]=duplicate_positive_samples(Feat(:,IndConf),Cl(IndConf),opts.ndup);
    Feat=[FeatTrainDup,FeatConfDup,FeatTest];
    Cl=[ClTrainDup;ClConfDup;ClTest];    
    N=size(Feat,2);    
    n_train = size(FeatTrainDup,2);
    n_Conf = size(FeatConfDup,2);
    n_test=N-n_train-n_Conf;
    Indices=[1:N];
    IndTrain=Indices(1:n_train);
    IndConf=sort( Indices((n_train+1):(n_train+n_Conf)) );
    IndTest=sort( Indices((n_train+n_Conf+1):end) );
    if (NCl==2)
        ClProb=zeros(NCl,N);
        ClProb(1,logical(Cl))=1;
        ClProb(2,:)= -ClProb(1,:)+1;
        ClProb=sparse(ClProb);
    else
        ClProb=ind2vec(Cl');
    end
end

if (opts.Nrm)
    %normalize Features
    rng=[-.5,.5]; % this is the range
    %on the x-axis where we want most of our data
    %to be located
    drng=rng(2)-rng(1);
    pbt=10; ptp=90; % percentage of top and bottom quantile
    for j=1:size(Feat,1)
        q=[prctile(Feat(j,IndTrain),pbt) , prctile(Feat(j,IndTrain),ptp)];
        dq=q(2)-q(1);
        a=drng/dq;
        b=(q(2)*rng(1)-q(1)*rng(2))/dq;
        Feat(j,:)=sigm(a*Feat(j,:)+b);
    end
end

%permute indices in order
%that the classes are uniformely distributed
if opts.PermCl
ind=permute_Feat(Cl);
Cl=Cl(ind);
Feat=Feat(:,ind);
end
DataStat.N=N;
DataStat.train=n_train;
DataStat.test=n_test;
DataStat.conf=n_Conf;
DataStat.pos=sum(Cl==ClMainDense)/DataStat.N;
DataStat.postrain=sum(Cl(IndTrain)==ClMainDense)/DataStat.train;
DataStat.postest=sum(Cl(IndTest)==ClMainDense)/DataStat.test;
DataStat.NCl=NCl;
