% Yitzhak-version of pnn (prob neur net)
%P: (dim_inp x 1) input
%W1:(ntrain x dim_inp)  first layer weights: neuron positions
%b1: bias: determines width of the exponentials 
%W2:(num_cl x ntrain) second layer weights : lables matrix
    % has 1 in row corresponding to class and 0 in all other rows
function Conf=pnn_ys(p,W1,b1,W2)
dists=dist(W1,p);
gdists=radbas(b1*dists);
Conf=(W2*gdists)/sum(gdists);
