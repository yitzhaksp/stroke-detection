function [Prd,scores]=Classification_KNearNeig_fun(Feat,Cl,partition,opts)
IndTrain=partition.IndTrain;
IndConf=partition.IndConf;
IndTest=partition.IndTest;
PlotStat=0;
PlotConf=0;
CompConf=0;
CompStatwrtConf=0; % calc statistics wrt Confidence

dummy=0;
opts1.weight=2; % weights: 1-equal, 2-gaussian dist
opts1.conf=2; %confidence type 1-correct, 2-positive
%calc confidence of sample beeing true
scores=ClassifyKNearNeig(Feat(:,IndTest),Feat(:,IndTrain),dummy,Cl(IndTrain),opts.nNeig,opts1);
scores=scores(1,:);
Prd=zeros(size(scores))';
switch opts.trh_Flag
    case 0
        trh_cls=.5;
    case 1
        trh_cls=opts.trh_cls;
    case 2
        scores_IndConf=ClassifyKNearNeig(Feat(:,IndConf),Feat(:,IndTrain),dummy,Cl(IndTrain),opts.nNeig,opts1);
        scores_IndConf=scores_IndConf(1,:);
        trh_cls=calibrate_treshhold_for_probabilities_fun(Cl(IndConf),scores_IndConf');        
end
Prd(scores>=trh_cls)=1;
%fprintf('\n num_feat=%d \n num_neighb=%d',nFeatAct,nNeig);
%-------------------------------------------------------
%% compute certainty level confLMH
a=[.1, .7,100]; % params for ConfLMH
%LMH defines low, medium and high confidence boundaries:
LMH=[.5,.8];
if CompConf
    %compute Confidence of correct classification on IndTest
    %based on the predictions of the classifier on IndConf
    nNeigConf=10;
    opts.weight=1; % weights: 1-equal, 2-gaussian dist
    ConfC=calc_ConfC(Feat(:,IndTest),Feat(:,IndConf),PrdConf,Cl(IndConf),nNeigConf,opts.weight);
    [maxx,ConfLMH]=calc_ConfLMH(n_test,a,LMH,ConfC,ConfP,nNeigConf);
end
% end compute certainty level
%--------------------------------------------------------
% compute statistics with respect to the tresholds of ConfC
if (CompStatwrtConf)
    h=.1;
    %trh=[0:h:1];
    trh=[0,LMH,1];
    % statistics with respect to the tresholds of ConfC
    % for the following we assume that every index in IndTest2
    % as greater than every index in IndConf
    clear ind
    for i=1:length(trh)
        ind(i,:)=(ConfC>=trh(i));
    end
    stats=comp_stat1(Prd,Cl(IndTest),ind);
    clear ind
    % statistics with respect to the tresholds of ConfP
    for i=1:(length(trh)-1)
        for j=1:n_test
            ind(i,j)=(ConfP(1,j)>=trh(i)) && (ConfP(1,j)<=trh(i+1));
        end
    end
    stats1=comp_stat1(Prd,Cl(IndTest),ind);
    for i=1:length(trh)
        ind(i,:)=(ConfP(2,:)>=trh(i));
    end
    stats2=comp_stat1(Prd,Cl(IndTest),ind);
    clear ind
    % statistics with respect to confLMH
    ind(1,:)=(ConfLMH==1);
    ind(2,:)=(ConfLMH==2);
    ind(3,:)=(ConfLMH==3);
    stats3=comp_stat1(Prd,Cl(IndTest),ind);
end
if (PlotStat)
    %{
    subplot(3,2,1), plot(trh,stats.acr_trh,'*'); title('accuracy, x-values:ConfC>=x');grid on;
    subplot(3,2,2), plot(trh,stats.PPV_trh,'*'); title('PPV, x-values:ConfC>=x'); grid on;
    subplot(3,2,3), plot(trh,stats.NPV_trh,'*'); title('NPV, x-values:ConfC>=x');grid on;
    subplot(3,2,4), plot(trh,stats1.true_trh,'*'); title('true, x-values:ConfTr>=x');grid on;
    %}
    tmp=[stats1.true_ind;stats3.true_ind]';
    figure;bar(tmp); title(['blue=ConfP, red=ConfPInt, LMH=[' num2str(LMH(1)) ', ' num2str(LMH(2)) ']']);grid on;
end
if (PlotConf)
    subplot(2,1,1), hist(cnf.ConfP(1,:));
    subplot(2,1,2), hist(cnf.ConfN(1,:));
end