%{
%inputs
Feat,Cl: all features/classes (Train+Conf+Test)
IndTrainAF: training set for calculation of artificial features
%}

function [FeatML,stats]=calc_artificial_features(Feat,Cl,IndTrainAF,IndTest,params)
N=length(Cl);
IndNotTrainAF=setdiff([1:N],IndTrainAF);
k=1;
ind=[1:length(IndTest)];
if (params.svm)
svmparams.CV_gamma=0; %crossvalidation method for model parameter selection (gamma)
svmparams.CV_C=0; %crossvalidation method for model parameter selection (C)
Prd=[]; prob_estimates=[];
[Prd(IndNotTrainAF),prob_estimates(IndNotTrainAF,:)]=Classify_SVM(Feat(:,IndNotTrainAF),Feat(:,IndTrainAF),Cl(IndTrainAF),Cl(IndNotTrainAF),svmparams);
%[Prd,prob_estimates]=Classify_SVM(Feat(:,IndNotTrainAF),Feat(:,IndTrainAF),Cl(IndTrainAF),Cl(IndNotTrainAF),svmparams);
FeatML(k,IndNotTrainAF)=prob_estimates(IndNotTrainAF,1)';
stats_tmp=comp_stat1(Prd(IndTest)',Cl(IndTest),ind);
stats_tmp.Classifier='svm';
stats(k)=stats_tmp;
k=k+1;
end
if (params.knn)
nNeig=20; % num of neighbours
dummy=0;
opts.weight=2; % weights: 1-equal, 2-gaussian dist
opts.conf=2; %confidence type 1-correct, 2-positive 
%calc confidence of sample beeing true
ConfP(:,IndNotTrainAF)=ClassifyKNearNeig(Feat(:,IndNotTrainAF),Feat(:,IndTrainAF),dummy,Cl,nNeig,opts);
FeatML(k,IndNotTrainAF)=ConfP(1,IndNotTrainAF);
% compute statistics
use_trh=0; % use treshholded knn?
trh_knn=.61; %treshhold for knn
Prd=zeros(size(ConfP(1,:)))';
if (use_trh)
    Prd(ConfP(1,:)>=trh_knn)=1;
else
    Prd(ConfP(1,:)>=.5)=1;
end
stats_tmp=comp_stat1(Prd(IndTest),Cl(IndTest),ind);
stats_tmp.Classifier='knn';
stats(k)=stats_tmp;
k=k+1;
end
for i=1:size(FeatML,1)
  for j=1:length(IndTrainAF)
    FeatML(i,IndTrainAF(j))=NaN;
  end
end
 

