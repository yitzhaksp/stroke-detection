%{
calculate length of confidence intervall for a given sample for ConfP (probability of sample beeing true).
The calculation is based on ConfC (confidence of classifier which is calculated using 
a Confidence-set IndConf) 

inputs:
ConfC: confidence of classifier on a given sample from the testset 
       which is calculated using a Confidence-set IndConf
nNeigConf: how many points are used to compute ConfC for a given sample 
a: function parameters
       a(1): basic length of the interval
       a(2): influence of ConfC. ConfC=a(3) will have the effect that the
             function will return the basic length (a(1)). the function
             output is an decreasing function of ConfC.
       a(3): influence of nNeigConf. a(3)=nNeigConf has the consequence
             that the weight of ConfC is .5 . The weight of ConfC
             is an increasing function of nNeigConf
             
%}
function lng=Lcnf(a,ConfC,nNeigConf)
%lng=a(2)*(1-ConfC)*tanh(nCONF/a(3));
x0=.57; % this is the point where tanh is about .5
scl=tanh(x0*nNeigConf/a(3)); % this is the weight for the influence of ConfC
lng=a(1) * exp( scl*tanh( (a(2)-ConfC)/(1-a(2))) );
 


