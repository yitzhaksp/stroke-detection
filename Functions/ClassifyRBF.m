%{
RBF neural network classification

input variables:
FeatLb-Labeled Featurevecs
Cl-Classes of FeatLb
Feat-Featurevecs to be classified
     Feat:(nFeat x nSamples)
gamma- width of gaussian
%}
function Conf=ClassifyRBF(Feat,FeatLb,Cl,gamma)
N=size(Feat,2);
W1=FeatLb';
if size(Cl,2)==1
    Cl=Cl';
end
Cl(Cl==0)=2; % Cl=0 should be only in case of binary classfication
W2=ind2vec(Cl);
for i=1:N
    Conf(:,i)=pnn_ys(Feat(:,i),W1,gamma,W2); %RBF-Net
end