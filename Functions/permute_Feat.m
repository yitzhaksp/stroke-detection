%{
permute indices so that
Cl(ind)=1,0,1,0,..,1,0,0,0,0,0, ...
or
Cl(ind)=1,0,1,0,..,1,1,1,1,1,1, ...
%}
function ind=permute_Feat(Cl)
ind0=find(Cl==0);
ind1=find(Cl==1);
sw=1;
k0=1;
k1=1;
for i=1:length(Cl)
    if (sw==1)
        if (k0<=length(ind0))
            ind(i)=ind0(k0);
            k0=k0+1;
        else
           ind(i)=ind1(k1);
           k1=k1+1;
        end
    else
        if (k1<=length(ind1))
            ind(i)=ind1(k1);
            k1=k1+1;
        else
           ind(i)=ind0(k0);
           k0=k0+1;
        end
    end
    sw=-sw;
end
