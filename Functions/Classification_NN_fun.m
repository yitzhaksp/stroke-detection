function [Prd,Scores,stats_AF]=Classification_NN_fun(Feat,Cl,ClProb,NCl,partition,opts)
IndTrain=partition.IndTrain;
IndConf=partition.IndConf;
IndTest=partition.IndTest;
if opts.MLFeatFlag
    IndTrainAF=IndTrain; % training set for computing processed features
    IndTrain=IndConf;    
    [FeatML,stats_AF]=calc_artificial_features(Feat,Cl,IndTrainAF,IndTest,opts.MLFeatInputs);
    Feat=FeatML;
end

CompConf=0; % 0-no, 1-bootstrp, 2-knearneig
PlotConf=0;
PlotPoints=0;
PlotStat=0;

%confidence params. relevant only if confindence is computed through
%the bootstrap method
bst_ratio=0.5; % bootstrap ratio (for confidence)
ntrain=length(IndTrain);
n_bst=bst_ratio*ntrain;
numof_bst=5; % num of bootstraps (for confidence)

% neural network parameters
opt.MultToBin=0; % transform multiclass classification to binary classification
opt.DummyClass=0; %bool,add Dummy class for first output neuron?
opt.n_neurons=[3,3]; % network structure (hidden layers)

%nnet = newff(Feat(:,IndTrain),ClProb(:,IndTrain),opt.n_neurons);

[Prd,Scores,nnet]=Classify_NN(Feat,Cl,ClProb,NCl,IndTrain,IndTest,opt);
%PrdConf=calc_prd_NN(Feat(:,IndConf),NCl,nnet,opt);

%calculate confidence measure
if (CompConf)
    switch CompConf
        case 1
            clear NN1BstOutp;
            D0=NN1Outp;
            nnet1Bst = newff(FeatTrain,ClProbTrain,n_neurons);
            nnet1Bst.trainParam.epochs=20;
            nnet1Bst.divideFcn='divideblock';
            nnet1Bst.divideParam.trainRatio=0.7;
            nnet1Bst.divideParam.valRatio=0.3;
            nnet1Bst.divideParam.testRatio=1-nnet1Bst.divideParam.trainRatio-nnet1Bst.divideParam.valRatio;
            for k=1:numof_bst
                tmp=randperm(n_train);
                IndBst=IndTrain( tmp(1:n_bst) );
                ClProbBst=ClProb(:,IndBst);
                FeatBst=Feat(:,IndBst);
                nnet1Bst=init(nnet1Bst);
                [nnet1Bst,tr] = train(nnet1Bst,FeatBst,ClProbBst);
                NN1BstOutp(k,:,:)=sim(nnet1Bst,Feat);
            end
            NN1BstOutpAv=squeeze(mean(NN1BstOutp));
            D4=zeros(NCl,N); % part of confidence measure
            for k=1:numof_bst
                tmp=(NN1BstOutpAv-squeeze(NN1BstOutp(k,:,:))  ).^2;
                if (DummyClass)
                    tmp(1,:)=[];
                end
                D4=D4+tmp;
            end
            D4=D4/(N-1);
            Conf=D0-D4;
            Conf=Conf(:,IndTest);
            Conf=Conf-min( 0 , min(min(Conf)) ); %make Conf>=0
            Conf=normalizze1(Conf); % make sum of each column = 1
            Conf=Conf(1,:);
        case 2
            PrdConf=calc_prd_NN(Feat(:,IndConf),NCl,nnet,opt);
            nNeigConf=10;
            opts.weight=1; % weights: 1-equal, 2-gaussian dist
            ConfC=calc_ConfC(Feat(:,IndTest),Feat(:,IndConf),PrdConf,Cl(IndConf),nNeigConf,opts.weight);
    end
    cnf=comp_stat_cnf(Cl(IndTest),ConfC);
end
%fprintf('\n num_feat=%d',nFeatAct);
%tmp=print_statistics(stats)
if (PlotStat)
    subplot(3,1,1), grid on, plot(trh,stats.acr,'*'); title('acr');grid on;
    subplot(3,1,2), plot(trh,stats.PPV,'*'); title('PPV'); grid on;
    subplot(3,1,3), plot(trh,stats.NPV,'*'); title('NPV');grid on;
end

if (PlotConf)
    subplot(2,1,1), hist(cnf.ConfP);
    subplot(2,1,2), hist(cnf.ConfN);
end

if (PlotPoints)
    maxx=2.0;
    Feattmp=Feat(:,(Cl==1));
    figure;plot(min(Feattmp(1,:),maxx),Feattmp(2,:),'*');
    hold on;
    for j=2:NCl
        Feattmp=Feat(:,find(Cl==j));
        plot(min(Feattmp(1,:),maxx),Feattmp(2,:),'*','color',rand(1,3));
        hold on;
    end
end