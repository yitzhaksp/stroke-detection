%{
costfunction for maximum likelihood estimation
of a model for probabilities of SVM Classification

from Article: Platt - 'probabilistic outputs of support vector machines and   	
comparisons ..' (page 4)

inputs:
f=f(xi): outputs of SVM on training samples xi: scaled distance from margin
t=t(xi): class of xi (0,1)
f and t must have same size
A,B: model parameters
%}
function cost=costfunc(A,B,f,t)
p=1./(1+exp(A*f+B));
cost=-(  dot(t,log(p)) + dot((1-t),log(1-p))  );