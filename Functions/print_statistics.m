function print_statistics(stats,opts)
fprintf('\n TPR=%.3f, TNR=%.3f',stats.TPR,stats.TNR);    
if (opts.A_trh)
fprintf(', A_trh=%.3f',stats.A_trh_ratio);    
end