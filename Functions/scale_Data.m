%{
scale data in such a way
that it is in [0,1] on the Trainset
the parameters of the scaling are computed
on the indTrain and the scaling is applied
on all indices
%}
function data_sc=scale_Data(data,indTrain)
    dataSc=zeros(size(data));
    for j=1:size(data,1)
        minnTrain=min(data(j,indTrain));
        rangeTrain=max(data(j,indTrain)-minnTrain);
        data_sc(j,:)=( data(j,:) - minnTrain )/rangeTrain;
    end
