%SVM classifier wrapper for cascade algorithm
function Conf=Clsf2(svmStruct, FeatTest)
[tmp,dist]= svmclassify1(svmStruct, FeatTest);
Conf=tanh(dist);
Conf=0.5*(Conf+1)';