%NN classifier wrapper for cascade algorithm
function Conf=Clsf3(nnet, FeatTest,DummyClass) 
Conf=sim( nnet , FeatTest );
if (DummyClass)
    Conf(1,:)=[];
end
Conf=normalizze1(Conf); % make sum of each column = 1
Conf=Conf(1,:);