function ConfC=calc_ConfC(FeatTest,FeatConf,PrdConf,ClConf,nNeig,weight)
opts.conf=1;
opts.weight=weight;
ConfC=ClassifyKNearNeig(FeatTest,FeatConf,PrdConf,ClConf,nNeig,opts);
