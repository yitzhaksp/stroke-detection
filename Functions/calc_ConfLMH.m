%{
calc Confidence-LMH (Low-Medium-High)

inputs:
a,ConfC,nNeigConf: see function Lcnf()
LMH: boundaries of low, medium and high sectors
ConfP: probability of sample beeing true
n_test: num of test samples   
%}
function [maxx,ConfLMH]=calc_ConfLMH(n_test,a,LMH,ConfC,ConfP,nNeigConf)
% length of confidence interval in one direction
% the length of the whole conidence intervall is twice this value
sigma_p=Lcnf(a,ConfC,nNeigConf); 
Confint=[ max(0 , ConfP(1,:) - sigma_p) ; min(1, ConfP(1,:) + sigma_p)];
%LMH defines low, medium and high confidence boundaries:

%calc the overlapps between the confidence intervall defined by ConfP
%and sigma_p and the low,medium and high intervals defined by LMH
for k=1:n_test
    ovlp(1,k)=overlapp(Confint(:,k),[0,LMH(1)]); %overlap low
    ovlp(2,k)=overlapp(Confint(:,k),[LMH(1),LMH(2)]); %overlap medium
    ovlp(3,k)=overlapp(Confint(:,k),[LMH(2),1]); %overlap high
end
%confcrt: certainty level of confidence: 1=low, 2=medium, 3=high
[maxx,ConfLMH]=max(ovlp);