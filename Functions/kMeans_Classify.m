%{
inputs:
p: sample
means: cluster-centers
scl: width of gaussian for each cluster
Cl: class of each cluster
%}
function Prd=kMeans_Classify(p,means,scl,Cl)
tmp1=scl.*dist(means,p);
weights=radbas(tmp1);
Prd=round( dot(weights,Cl)/sum(weights) );
