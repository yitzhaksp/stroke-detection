%Feature Selection
function FeatRank=Feature_Selection(Feat,Cl,FeatSelMethod)
% Core=1 means fixed features + selected features
Core=0;
    if (FeatSelMethod==1) %Shannon
        treshhold=0.2;
        discrete=zeros(1,size(FeatTrain,1));
        ShannonEntr = ShannonFeaturesTest(Feat, Cl, discrete, true, 10);
        [tmp1,FeatRank]=sort(ShannonEntr,'descend');
        %FeatActInd=find(ShannonEntr>treshhold);
        %nFeatAct=length(FeatActInd);
    elseif (FeatSelMethod==2)%mRMR
        FeatRank=mRMR(10*Feat,Cl);
    end
if (Core)
    nCore=length(FeatCoreInd);
    FeatActInd1=FeatCoreInd;
    k=nCore+1;
    for i=1:size(Feat,1)
        if (~ismember(FeatRank(i),FeatCoreInd))
            FeatActInd1(k)=FeatRank(i);
            k=k+1;
        end
    end
end
