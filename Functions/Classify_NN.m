function [Prd,Scores,nnet]=Classify_NN(Feat,Cl,ClProb,NCl,IndTrain,IndTest,opt)
nnet=Train_NN(Feat,Cl,ClProb,NCl,IndTrain,opt);
[Prd,Scores]=calc_prd_NN(Feat(:,IndTest),NCl,nnet,opt);   
       

   