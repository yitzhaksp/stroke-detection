% classify using libsvm
%inputs:
%Feat,Cl: test samples /classes
%FeatVal,ClVal: samples/classes for finding paprameters with CV

function [Prd,prob_estimates]=Classify_SVM(Feat,FeatTrain,ClTrain,Cl,params,varargin)
%% model parameter selection via CV
CV=params.CV_gamma+params.CV_C;
if (CV)
FeatVal=varargin{1};    
ClVal=varargin{2};
nfold=4;
n_val=length(ClVal);
IndCV=crossvalind('Kfold', n_val, nfold);
end
switch params.CV_gamma
    case 0
        load('gamma.mat');
    case 1 % select gamma from discrete set
        base=2.0;
        gammas=[base/16,base/8,base/4, base/2,base, 2*base, 4*base];

        %tmp1=[1:n_val];
        acrCV=zeros(1,length(gammas));
        %FeatVal=Feat(:,IndVal);
        for j=1:length(gammas)
            acrCV(j)=0;
            for i=1:nfold
                IndCVTest=(IndCV==i);
                IndCVTrain=~IndCVTest;
                options=['-s 0 -g ' num2str(gammas(j)) ' -b 1'];
                model = svmtrainl(ClVal(IndCVTrain), FeatVal(:,IndCVTrain)', options);
                [ClSim, acr, PrEst] = svmpredict(ClVal(IndCVTest),FeatVal(:,IndCVTest)', model);
                acrCV(j)=acrCV(j)+acr(1);
            end
            acrCV(j)=acrCV(j)/nfold;
        end
        [maxx,ind]=max(acrCV);
        gamma=gammas(ind);
    case 2 % find gamma by optimization
        c = cvpartition(n_train,'kfold',10);
        minfn = @(z)crossval('mcr',Feat(:,IndTrain)',ClTrain','Predfun', ...
            @(xtrain,ytrain,xtest)crossfunl(xtrain,ytrain,...
            xtest,exp(z)),'partition',c);
        opts = optimset('TolX',5e-4,'TolFun',5e-4);
        [searchmin fval] = fminsearch(minfn,randn(1,1),opts)
        gamma = exp(searchmin);

end
switch params.CV_C
    case 0
        load('C_lin.mat');
    case 1 % select gamma from discrete set
        base=2.0;
        Cs=[base/16,base/8,base/4, base/2,base, 2*base, 4*base];
        nfold=4;
        IndCV=crossvalind('Kfold', n_val, nfold);
        %tmp1=[1:n_val];
        acrCV=zeros(1,length(Cs));
        for j=1:length(Cs)
            acrCV(j)=0;
            for i=1:nfold
                IndCVTest=(IndCV==i);
                IndCVTrain=~IndCVTest;
                options=['-s 0 -g ' num2str(gamma) '-c' num2str(Cs(j)) ' -b 1'];
                model = svmtrainl(ClVal(IndCVTrain), FeatVal(:,IndCVTrain)', options);
                [ClSim, acr, PrEst] = svmpredict(ClVal(IndCVTest),FeatVal(:,IndCVTest)', model);
                acrCV(j)=acrCV(j)+acr(1);
            end
            acrCV(j)=acrCV(j)/nfold;
        end
        [maxx,ind]=max(acrCV);
        C=Cs(ind);
    case 2 % find gamma by optimization
        c = cvpartition(n_train,'kfold',10);
        minfn = @(z)crossval('mcr',Feat(:,IndTrain)',ClTrain','Predfun', ...
            @(xtrain,ytrain,xtest)crossfunl(xtrain,ytrain,...
            xtest,exp(z)),'partition',c);
        opts = optimset('TolX',5e-4,'TolFun',5e-4);
        [searchmin fval] = fminsearch(minfn,randn(1,1),opts)
        gamma = exp(searchmin);
end
    %% training
    %gamma=.1;
    % t=kernel: 0=lin, 1=poly, 2=radbas (default=2)
    % if you change t remember to load the right C (C_exp,C_lin)
    options=['-t 0 -s 0 -g ' num2str(gamma) '-c' num2str(C) ' -b 1'];
    model = svmtrainl(ClTrain, FeatTrain', options);
    %% testing
    [Prd, accuracy, prob_estimates] = svmpredict(Cl, ...
        Feat',model,' -b 1');