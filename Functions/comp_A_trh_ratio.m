function A_trh_ratio=comp_A_trh_ratio (Labels,scores,trh)
[X,Y]=perfcurve(Labels,scores,1);
ind_trh=(X<=trh);
Y_trh=Y(ind_trh);
dx=trh/length(Y_trh);
A_trh=dx*sum(Y_trh);
A_trh_ratio=(A_trh-.5*trh*trh)/(trh-.5*trh*trh);