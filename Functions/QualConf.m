%{
calculate the quality of a confidence measure based on the
LMH-true-percentages. a confidence measure has high quality (output=1.0)
if the LMH-true-percentages lie within the boundaries defined
by LMH. The return is an increasing function of the violation of
these boundaries by the LMH-true-percentages: (worst case: output=0.0)

inputs:
true_LMH: true percentages for Low, medium, high
LMH: low-medium-high boundaries (2 values)
%}
function y=QualConf(true_LMH,LMH)
%the feas_xx variables indicate wether true_LMH is where we want it to
%be in relation to LMH
tmp=max(true_LMH(1)-LMH(1),0)+ max(-true_LMH(2)+LMH(1),0)+max(true_LMH(2)-LMH(2),0)+max(-true_LMH(2)+LMH(2),0);
% the sclaling factor is the maximal possible violation produces by the
% sum above
scl=(1-LMH(1))+max(LMH)+LMH(2);
y=1-tmp/scl;