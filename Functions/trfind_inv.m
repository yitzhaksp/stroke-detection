%{
indbase: indices of subvec 
indvec: indices to be transformed

%}
function indsub=trfind_inv(indvec,indbase)
indsub=zeros(1,sum(indbase));
for i=1:length(indvec)
    if (indvec(i))
        if (indbase(i))
            tmp=indbase(1:i);
            indsub(sum(tmp))=1;
        else
            disp(i);
            error('index %d does not belong to subvec',i);
        end
    end
indsub=logical(indsub);    
end