%{
classification using libsvm
https://www.csie.ntu.edu.tw/~cjlin/libsvm/
%}
function [Prd,Prob]=Classification_SVM_fun(Feat,Cl,partition,opts)
%crossvalidation method for model parameter selection
% 0-load, 1-choose via CV
IndTrain=partition.IndTrain;
IndConf=partition.IndConf;
IndTest=partition.IndTest;
params.CV_gamma=0;
params.CV_C=0;
CompConf=0;
PlotConf=0;

[Prd,prob_estimates]=Classify_SVM(Feat(:,IndTest),Feat(:,IndTrain),Cl(IndTrain),Cl(IndTest),params,Feat(:,IndConf),Cl(IndConf));
Prob=prob_estimates(:,1);

%compute statistics relative to confidence
ConfStat=0;
if ConfStat
    nNeig=5;
    opts.weight=2; % weights: 1-equal, 2-gaussian dist
    opts.conf=1; %confidence type 1-correct, 2-positive
    opts.gamma=1.0; % width of gaussian
    ConfC=ClassifyKNearNeig(Feat(:,IndTest),Feat(:,IndTest),Prd,Cl(IndTest),nNeig,opts);
    h=.1;
    trh=[0:h:1];
    stats=comp_stat1(Prd,Cl(IndTest),ConfC,trh,ClMainDense);
end

%% Compute Confidence
if (CompConf)
    Conf=zeros(NCl,n_test);
    if (model.nr_class==2)
        tmp1=find(model.Label==1);
        tmp2=mod((tmp1-1),2)+1; % tmp1=1->tmp2=2; tmp1=2->tmp2=1
        Conf(1,:)=prob_estimates(:,tmp1)';
        Conf(2,:)=prob_estimates(:,tmp2)';
    else
        for i=1:model.nr_class
            Conf(model.Label(i),:)=prob_estimates(:,i)';
        end
    end
    Conf=Conf';
    if (PlotConf)
        subplot(2,1,1), hist(Conf(Prd==Cl(IndTest)));
        subplot(2,1,2), hist(Conf(Prd~=Cl(IndTest)));
    end
end

