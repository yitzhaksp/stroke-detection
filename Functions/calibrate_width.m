%{
calibrates sclaling parameter for the distance between
center of a cluster and points in the cluster such that
a certain percentage of the points lies within a certain
scaled distance from the center

input variables:
cnc: concentration (percentage of points)
trh: within this distance 
cntr= cluster-center
%}
function sclopt=calibrate_width(cntr,x,cnc,trh)
tmp1=dist(cntr,x);
fnc=@(scl) ( prctile(scl*dist(cntr,x),cnc) - trh )^2;
opts = optimset('TolX',5e-4,'TolFun',5e-4);
scl0=1.0;
[sclopt fval]=fminsearch(fnc,scl0,opts);
