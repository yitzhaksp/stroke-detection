%{
calc statistics on subsets
remember that the labeled set is dividid into 3 sets:
IndTrain+IndTest where IndTest=IndTest1+IndTest2
inputs:
Prd: predictions of some classifier on IndTest2
Cl:groundtruth on IndTest2
ConfC: Confidence of correct classification on IndTest2.
       if we want to calculate relative to tresholds of ConfTr (conf of sample being true)
       then ConfC has the meaning of ConfTr and IndTest2 shall be =IndTest.
ind: indizes: (nsubsets x length(Cl))
%}
function stat=comp_stat1(Prd,Cl,ind,varargin)
for i=1:size(ind,1)
    if (~isempty(varargin))
        tmp1=comp_stat(Prd(ind(i,:)),Cl(ind(i,:)),varargin{1});
    else        
        tmp1=comp_stat( Prd(ind(i,:)) , Cl(ind(i,:)) );
    end
    stat.n_ind(i)=tmp1.n;
    stat.acr_ind(i)=tmp1.acr;
    stat.acr_w_ind(i)=tmp1.acr_w;
    stat.TPR_ind(i)=tmp1.TPR;
    stat.TNR_ind(i)=tmp1.TNR;
    stat.PPV_ind(i)=tmp1.PPV;
    stat.pos_ind(i)=tmp1.pos;
end