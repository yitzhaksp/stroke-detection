 function [Prd,Scores]=calc_prd_NN(Feat,NCl,nnet,opt)   
 N=size(Feat,2);   
 NN1Outp=sim(nnet,Feat);
    if (opt.DummyClass)
        NN1Outp(1,:)=[];
    end
    if (opt.MultToBin && (NCl>2.1))   
            tmp=zeros(2,N);
            tmp(1,:)=NN1Outp(opt.ClMainDense,:);
            tmp1=[ 1:(opt.ClMainDense-1) , (opt.ClMainDense+1):NCl ];
            tmp(2,:)=max(NN1Outp(tmp1,:));
            NN1Outp=tmp;
      
    end
    for i=1:N
        [maxentry,Prd(i)]=max(NN1Outp(:,i));
        Scores(i)=NN1Outp(1,i);
        %NN1OutpNrm(:,i)=normalizze(NN1Outp(:,i));
        %NN1OutpNrm(:,i)=NN1OutpNrm(:,i)/sum(NN1OutpNrm(:,i));
    end
    if (NCl==2)
        Prd(Prd==2)=0;
    end
