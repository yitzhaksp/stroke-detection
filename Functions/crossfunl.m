function ClPred = ...
    crossfunl(xtrain,ytrain,xtest,rbf_sigma)

% Train the model on xtrain, ytrain, 
% and get predictions of class of xtest
options=['-s 0 -g ' num2str(rbf_sigma)];
ytrain=double(ytrain);
svmStruct = svmtrainl(ytrain,xtrain,options);
ClDummy=zeros(size(xtest,1),1);
[ClPred, acr, PrEst] = svmpredict(ClDummy,xtest,svmStruct);
