function I = ContinuesShannon(feature, labels, useBalance)

%% Description: Discrete Shannon test for features
%%
%%
%% Parameters:  feature    - N*1 matix of N objects with a discrete feature
%%              labels      - an labeled array (classes) of the given
%%              objects
%%             
%% Return:      I           - the shannon value
%%

I = -1;

%calculate variance, mean, max/min for the current feature
fvariance = sqrt(var(feature));
fmean = mean(feature);
maxval = max(feature);
minval = min(feature);

%find gragation size
grag1.min = minval;
grag1.max = fmean - fvariance;
grag2.min = fmean - fvariance;
grag2.max = fmean + fvariance;
grag3.min = fmean + fvariance;
grag3.max = maxval;


%find I for three equal gragations:
I_3grag_equal = Shannon_3EqualGragation(feature, labels, useBalance);


%find I for three gragation using 2sigma segments
I_3grag_with_2sigma = -1;
if (fmean - fvariance > minval)
    I_3grag_with_2sigma = Shannon_3Gragation(feature, labels, grag1, grag2, grag3, useBalance);
end


%find I for two equal segments
I_2grags_equal = Shannon_2EqualGragation(feature, labels, useBalance);


%return the highest score of I
I = max([I_3grag_equal, I_3grag_with_2sigma, I_2grags_equal]);

end