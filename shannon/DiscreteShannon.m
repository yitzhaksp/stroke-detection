function I = DiscreteShannon(feature, labels, useBalance)

%% Description: Discrete Shannon test for feature
%%
%%
%% Parameters:  feature    - N*1 matix of N objects with a discrete feature
%%              labels      - an labeled array (classes) of the given
%%              objects
%%
%% Return:      I           - the shannon value
%%

%we make one gragation per each class
enums = unique(feature);
grag_num = size(enums,1);
classes = unique(labels);

sizes = [];
total = 0;
for i = 1:grag_num
    sizes = [sizes size(find(feature == enums(i)), 1)];    
    total = total + sizes(i);
end

probGrags = [];
for i = 1:grag_num
    prob = (sizes(i) + 1) / (total + grag_num);  
    probGrags = [probGrags prob];
end

%calculate the h_prior and h_post for all gragation
h_prior = 0;
h_post = zeros(1, grag_num);
prob_i_xj = zeros(grag_num, size(classes,1));
for c = 1:size(classes,1)

    %calcluate the prior probability for class c:
    ic = find(labels == classes(c));
    prior_c = (size(ic,1) + 1) / (total + size(classes,1));%bias estimation for small statistics (instead of size(ic,1)/total)
    h_prior = h_prior - prior_c * (log2(prior_c) / log2(size(classes,1)));

    %calculate the feature of class c:
    c_feature = feature(ic);

    for gi = 1:grag_num
        
        %calculate h_post for class c:
        grag_c = find(c_feature == enums(i));
        prob_xj_i = (size(grag_c,1) + 1) / (size(ic,1) + 3);%bias estimation for small statistics (instead of size(grag1_c,1) / size(ic,1))
        if useBalance
            prob_i_xj(gi,c) = (prob_xj_i / probGrags(i));%bias formula with balancing method
        else
            prob_i_xj(gi,c) = (prior_c * prob_xj_i) / probGrags(i);%bias formula
            h_post(gi) = h_post(gi) - prob_i_xj(gi,c) * (log2(prob_i_xj(gi,c)) / log2(size(classes,1)));
        end
    end


end

if useBalance
    h_prior = 1;
    
    for gi = 1:grag_num
        
        prob_i_xj(gi,:) = prob_i_xj(gi,:) ./ sum(prob_i_xj(gi,:));%normalize per each gragation
        
        for c = 1:size(classes,1)
            h_post(gi) = h_post(gi) - prob_i_xj(gi,c) * (log2(prob_i_xj(gi,c)) / log2(size(classes,1)));%calculate post anthropy for the current gragation
        end
    end
   
end

I_grags = repmat(h_prior, 1, grag_num);
I_grags = I_grags - h_post;
I_grags = I_grags.*probGrags;
I = sum(I_grags);



