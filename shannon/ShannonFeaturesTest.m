function I = ShannonFeaturesTest(features, labels, discrete, useBalance, numberOfGragationIntervals)

%% Description: Shannon test for features
%%
%%
%% Parameters:  features    - N*M matix of N objects with M features
%%              labels      - an labeled array (classes) of the given objects
%%              discrete    - an array which indicate which features are
%%                            descrete ('1' is descrete, '0' other wise)
%%             
%% Return:      I           - 1*M dimention array which consist the shannon value for each one of the M features
%%

I = [];

classes = unique(labels);

for i = 1:size(features,2)
    
    %current feature
    curf = features(:,i);
    
    disp('----------------------------------');
    fprintf('calculate I for feature %d\n', i);
    
    if (discrete(i))
        I_f = DiscreteShannon(curf, labels, useBalance);
    else
        I_f = ContinuesShannon(curf, labels, useBalance, numberOfGragationIntervals);
    end    
    
    I = [I I_f]
end




