function dispString = calcShannonPerPairs(curf,labels,featureNames,dispIt,sortIt)

if nargin<4
    dispIt =0;
end 
if nargin<5
    sortIt =1;
end 

nFeatures = size(curf,2);
lab = unique(labels);
nClasses = max(size(lab));

if nargin<3
    featureNames={};
    for k = 1:nFeatures
        featureNames{k} ={num2str(k)};
    end
end 

kk = 1;
for k = 1:nFeatures
    dispString ={['Shannon test for Feature ' char(featureNames{k}) ' < class1,class2, score >: ']} ;
    shanTests = [];
    classes = [];
    for i = 1:nClasses
        for j= i+1: nClasses
            F = [curf((labels == lab(i)),k) ; curf((labels == lab(j)),k)];
            L = [labels((labels == lab(i))) ; labels((labels == lab(j)))];
            shanTests = [shanTests  ContinuesShannon(F, L, true, 10)];
            classes = [ classes  ; [ i j]];
        end
    end
    S=[];
    if sortIt
        [shanTests,I]=sort(shanTests);
        shanTests = fliplr(shanTests);
        I = fliplr(I);
        classes=classes(I,:);
    end
    for ii = 1: size(classes,1)
        %    S = [S '<' num2str(lab(classes(ii,1))) ',' num2str(lab(classes(ii,2))) '>  score: '  num2str(shanTests(ii)) '; ' ];
        %S = [S '<' num2str(lab(classes(ii,1))) ' , ' num2str(lab(classes(ii,2))) ' , '  num2str(shanTests(ii),2) '>' ];
        kk=kk+1;
        if sortIt
            dispString{kk} = {['< ' num2str(lab(classes(ii,1))) ' , ' num2str(lab(classes(ii,2))) ' , '  num2str(shanTests(ii),2) ' >;' ]};
        else
            dispString{kk} = {[', ' num2str(lab(classes(ii,1))) ' , ' num2str(lab(classes(ii,2))) ' , '  num2str(shanTests(ii),2) ' ;' ]};
        end        
    end
    %dispString{k} = {['Shannon test for Feature ' char(featureNames{k}) ' <class1,class2,score>: '   S]};
    if dispIt
        disp(dispString);
    end
end