function [trainData, trainClass, validData, validClass] = AMAT_Generate_Data()

trainData = [];
trainClass = [];
validData = [];
validClass = [];

%startup 
randn('state',sum(100*clock)); 

DIM = 5;

TRAIN_SIZE = 1000;
VALID_SIZE = 1000;


M1 = [17 17 17 17 17];
M2 = [6 6 6 6 6];

COV_1 = [1  0   0   0   0
         0  1   0   0   0
         0  0   1   0   0
         0  0   0   1   0
         0  0   0   0   1];
COV_2 = [1  0   0   0   0
         0  1   0   0   0
         0  0   1   0   0
         0  0   0   1   0
         0  0   0   0   1];
     

gmm1 = gmm(DIM, 1, 'full');
gmm1.centres = M1;
gmm1.priors(1) = 1;
gmm1.covars(:,:,1) = COV_1;

gmm2 = gmm(DIM, 1, 'full');
gmm2.centres = M2;
gmm2.priors(1) = 1;
gmm2.covars(:,:,1) = COV_2;



for i = 1:TRAIN_SIZE
    if (randn < 0)
        trainData = [trainData 
                    gmmsamp(gmm1, 1)];      
        trainClass = [trainClass 
                      0]; 
    else
        trainData = [trainData 
                    gmmsamp(gmm2, 1)];
        trainClass = [trainClass 
                      1]; 
    end
end

for i = 1:VALID_SIZE
    if (randn < 0)
        validData = [validData 
                    gmmsamp(gmm1, 1)];      
        validClass = [validClass 
                      0]; 
    else
        validData = [validData 
                    gmmsamp(gmm2, 1)];
        validClass = [validClass 
                     1]; 
    end
end


        
%trainData = gmmsamp(gmm1, TRAIN_SIZE);
%validData = gmmsamp(gmm2, VALID_SIZE);