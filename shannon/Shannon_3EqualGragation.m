function I = Shannon_3EqualGragation(curf, labels, useBalance)

%% Description: Shannon test for features
%%
%%
%% Parameters:  features    - N*M matix of N objects with M continues features
%%              labels      - the labels (classes) of the given objects
%%             
%% Return:      I           - 1*M dimention array which consist the shannon value for each one of the M features
%%

I = [];

classes = unique(labels);

%find gragation size
maxval = max(curf);
minval = min(curf);
gragationSize = (maxval - minval) / 3;
% fprintf('3 equal gragation: min=%d, max=%d, gradation size=%d\n', minval, maxval, gragationSize);

fstGragIdx = find(curf < minval + gragationSize);
scdGragIdx = find((curf >= (minval + gragationSize)) & (curf <= (minval + 2 * gragationSize)));
trdGragIdx = find(curf > (minval + 2 * gragationSize));

size1 = size(fstGragIdx,1);
size2 = size(scdGragIdx,1);
size3 = size(trdGragIdx,1);
total = size1 + size2 + size3;

%bias estimation for small statistic (instead of size/total)
probGrag1 = (size1 + 1) / (total + 3);
probGrag2 = (size2 + 1) / (total + 3);
probGrag3 = (size3 + 1) / (total + 3);

%calculate the h_prior and h_post for all gragation
h_prior = 0;
h1_post = 0;
h2_post = 0;
h3_post = 0;

prob1_i_xj = [];
prob2_i_xj = [];
prob3_i_xj = [];

for c = 1:size(classes,1)


    %calcluate the prior probability for class c:
    ic = find(labels == classes(c));
    prior_c =  (size(ic,1) + 1) / (total + size(classes,1));%bias estimation for small statistics (instead of size(ic,1)/total)
    h_prior = h_prior - prior_c * (log2(prior_c) / log2(size(classes,1)));

    %calculate the feature of class c:
    c_features = curf(ic);

    %calculate h1_post for class c:
    grag1_c = find(c_features < minval + gragationSize);
    prob1_xj_i = (size(grag1_c,1) + 1) / (size(ic,1) + 3);%bias estimation for small statistics (instead of size(grag1_c,1) / size(ic,1))
    if useBalance
        prob1_i_xj = [prob1_i_xj (prob1_xj_i / probGrag1)];%bias formula with balancing method
    else
        prob1_i_xj = [prob1_i_xj (prior_c * prob1_xj_i) / probGrag1];%bias formula
        h1_post = h1_post - prob1_i_xj(c) * (log2(prob1_i_xj(c)) / log2(size(classes,1)));
    end

    %calculate h2_post for class c:
    grag2_c = find((c_features >= (minval + gragationSize)) & (c_features <= (minval + 2 * gragationSize)));
    prob2_xj_i = (size(grag2_c,1) + 1) / (size(ic,1) + 3);%bias estimation for small statistics
    if useBalance
        prob2_i_xj = [prob2_i_xj (prob2_xj_i / probGrag2)];%bias formula with balancing method
    else
        prob2_i_xj = [prob2_i_xj (prior_c * prob2_xj_i) / probGrag2];%bias formula
        h2_post = h2_post - prob2_i_xj(c) * (log2(prob2_i_xj(c)) / log2(size(classes,1)));
    end

    %calculate h3_post for class c:
    grag3_c = find(c_features > (minval + 2 * gragationSize));
    prob3_xj_i = (size(grag3_c,1) + 1) / (size(ic,1) + 3);%bias estimation for small statistics
    if useBalance
        prob3_i_xj = [prob3_i_xj (prob3_xj_i / probGrag3)];%bias formula with balancing method
    else
        prob3_i_xj = [prob3_i_xj (prior_c * prob3_xj_i) / probGrag3];%bias formula
        h3_post = h3_post - prob3_i_xj(c) * (log2(prob3_i_xj(c)) / log2(size(classes,1)));
    end

end

if useBalance
    h_prior = 1;
    
    prob1_i_xj = prob1_i_xj ./ sum(prob1_i_xj);
    prob2_i_xj = prob2_i_xj ./ sum(prob2_i_xj);
    prob3_i_xj = prob3_i_xj ./ sum(prob3_i_xj);
    
    for c = 1:size(classes,1)
        h1_post = h1_post - prob1_i_xj(c) * (log2(prob1_i_xj(c)) / log2(size(classes,1)));
        h2_post = h2_post - prob2_i_xj(c) * (log2(prob2_i_xj(c)) / log2(size(classes,1)));
        h3_post = h3_post - prob3_i_xj(c) * (log2(prob3_i_xj(c)) / log2(size(classes,1)));
    end
    
end

I_1 = h_prior - h1_post;
I_2 = h_prior - h2_post;
I_3 = h_prior - h3_post;

I = probGrag1*I_1 + probGrag2*I_2 + probGrag3*I_3;





