function I = ContinuesShannon(feature, labels, useBalance, numberOfGragationIntervals)

%% Description: Discrete Shannon test for features
%%
%%
%% Parameters:  feature                     -    N*1 matix of N objects with a discrete feature
%%              labels                      -    an labeled array (classes) of the given objects
%%              useBalance  -                   whether we should simulate balance data by
%%                                              placing 1 in the prior enthropy value.
%%              numberOfGragationIntervals  -   Number of gragations per each features;
%%             
%% Return:      I                   `       -    the shannon value
%%

if (numberOfGragationIntervals < 4)
    I = RegularContinuesShannon(feature, labels, useBalance);
    return;
end

I_all = [];

maxval = max(feature);
minval = min(feature);
gragSize = (maxval - minval) / numberOfGragationIntervals;

for i = 1:(numberOfGragationIntervals - 1)

    grag1.min = minval;
    grag1.max = minval + gragSize * i;
    grag2.min = minval + gragSize * i;
    grag2.max = maxval;

    I_all = [I_all Shannon_2Gragation(feature, labels, grag1, grag2, useBalance)];

end

for i = 1:(numberOfGragationIntervals - 2)
    for j = (i + 1):(numberOfGragationIntervals - 1)

        grag1.min = minval;
        grag1.max = minval + gragSize * i;
        grag2.min = minval + gragSize * i;
        grag2.max = minval + gragSize * j;
        grag3.min = minval + gragSize * j;
        grag3.max = maxval;

        I_all = [I_all Shannon_3Gragation(feature, labels, grag1, grag2, grag3, useBalance)];
    end
end

%return the highest score of I
I = max(I_all);


