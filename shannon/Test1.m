
clear all;
[trainData trainClass validData validClass] = AMAT_Generate_Data;

rand('state',sum(100*clock));

bin_f1 = floor(2.*rand(size(trainData,1),1));
bin_f2 = ceil(2.*rand(size(trainData,1),1));
enum_f1 = ceil(5.*rand(size(trainData,1),1));
enum_f2 = ceil(5.*rand(size(trainData,1),1));

discrete = zeros(1, size(trainData,2));
discrete = [discrete 1 1 1 1];
trainData = [trainData bin_f1 bin_f2 enum_f1 enum_f2];



result = ShannonFeaturesTest(trainData, trainClass, discrete, true, 10)