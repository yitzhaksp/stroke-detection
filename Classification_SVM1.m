% Classification using MatLab's SVM algorithm
% this svm-program was not updated for long time
% use Classification_SVM instead
LoadData=1;
addpath(genpath('.'));
addpath('../useful_functions');
if (LoadData)
   clear all
    set_loadparams;
    [Feat,Cl,ClProb,NCl,IndTrain,IndConf, ...
     IndTest,DataStat]=Load_and_Separate(DataFlag,opts);
    nFeatAct=7; % num of active features    
    LoadFeatRank=1; % load featurerank 
    if LoadFeatRank
        switch DataFlag
            case 1
                load('FeatRank_aug14');
            case 2
                load('FeatRank_jul15');
        end
    else
        FeatSelMeth=2; %1:shannon, 2:mrmr
        FeatRank=Feature_Selection(Feat(:,IndTrain)',Cl(IndTrain),FeatSelMeth);
    end
    FeatAct=FeatRank(1:nFeatAct);
    Feat=Feat(FeatAct,:);
end
CompConf=3; %0=no, 1=bin-based, 2=distance-based, 3-knearneig
PlotStat=1; 
opts.Cases=0; % classify cases?
opts.ParamsFlag=3; %1-CV, 2-load, 3-set
opts.Kernel='rbf';
params.C=1.0;
params.sigma=1.0;
[Prd,dist,SVMstruct]=Classify_SVM1(Feat(:,IndTest),Feat(:,IndTrain),Cl(IndTrain),opts,params);
[PrdTrain,distTrain] = svmclassify1(SVMstruct,Feat(:,IndTrain)','showplot',true);
%calc confidence
if (CompConf)
    switch CompConf
        case 1
            %code pieces: 2
        case 2
            Conf=.5*(tanh(dist)+1);
        case 3
            nNeig=10;
            opts.weight=1; % weights: 1-equal, 2-gaussian dist
            opts.conf=1; %confidence type 1-correct, 2-positive 
            opts.gamma=1.0; % width of gaussian
            Conf=ClassifyKNearNeig(Feat(:,IndTest),Feat(:,IndTest),Prd,Cl(IndTest),nNeig,opts);
    end
    cnf=comp_stat_cnf(Cl(IndTest),Conf);
    subplot(2,1,1), hist(cnf.ConfP);
    subplot(2,1,2), hist(cnf.ConfN);
end
%compute statistics
h=.1;
trh=[0:h:1];
ind=[1:length(IndTest)];
stats=comp_stat1(Prd,Cl(IndTest),ind);
if (PlotStat)
    subplot(3,1,1), grid on, plot(trh,stat.acr,'*'); title('acr');grid on;
    subplot(3,1,2), plot(trh,stat.PPV,'*'); title('PPV'); grid on;
    subplot(3,1,3), plot(trh,stat.NPV,'*'); title('NPV');grid on;
end

