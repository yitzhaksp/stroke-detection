IndTrain=partition_out.IndTrain;
IndTest=partition_out.IndTest;
Mdl = fitensemble(Feat(:,IndTrain)',Cl(IndTrain),'AdaBoostM1',100,'Tree');
Prd = predict(Mdl,Feat(:,IndTest)');
stats=comp_stat(Prd,Cl(IndTest));
opts2.A_trh=0;
print_statistics(stats,opts2);