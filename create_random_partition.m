N=2310;
train_ratio=.7;
conf_ratio=.01;
n_train=floor(N*train_ratio);
n_conf=floor(N*conf_ratio);
n_test=N-n_train-n_conf;
num_partitions=20;
for k=1:num_partitions
ind=randperm(N);
partitions(k).trainIdx=ind(1:n_train);
partitions(k).confIdx=ind(n_train+1:n_train+n_conf);
partitions(k).testIdx=ind((n_train+n_conf+1):end);
end
save('partitions.mat','partitions');