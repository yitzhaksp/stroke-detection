function opts=set_loadparams()
opts.DataPath='Data_Sep15';
opts.FeatRankPath='FeatRank_sep15';
 opts.new_perm_FLAG=3;
    opts.Nrm=1; %Normalize Feat?
opts.train_ratio=.7;
opts.conf_ratio=.15;
opts.new_perm_FLAG=3; %how to choose training samples: 1-1:ntrain, 2-rand, 3-load 
opts.ClBinFlag=1; %bool, transform multiple labeling to dual
opts.ClMain=1; %if ClBinFlag throw all classes except this into one class
opts.ClDenseFlag=1; %densify Label indices so that labels                  % are {1,2,..,NCl} (without gaps)
opts.ClChooseFlag=0; %bool,choose only data from certain classes
opts.ChosenLabels=[3,5,8,10,11]; %if ClChooseFlag: choose data from these classes
opts.PermCl=0; %permute Cl (and Feat respectively)
opts.DupPos=1; %duplicate positive samples in order to have balanced training set?
opts.ndup=6; %number of duplicates (relevant only if DupPos=true)