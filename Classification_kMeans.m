LoadData=0;
if (LoadData)
    Load_and_Separate;
    Feature_Selection;
end
nMeans=5; %nMeans for each class
ind=(ClTrain==1);
[tmp,Cnt1]=kmeans((FeatTrain(:,ind))',nMeans);
ind=(ClTrain==2);
[tmp,Cnt2]=kmeans((FeatTrain(:,ind))',nMeans);
Cnt=[Cnt1;Cnt2];
ClCnt=ones(size(Cnt,1),1);
ClCnt((nMeans+1):end)=2;
for i=1:size(Cnt,1);
    scl(i)=calibrate_width(Cnt(i,:),FeatTest,0.9,1.2);
end
for k=1:n_test
    Prd(k)=kMeans_Classify(FeatTest(:,k),Cnt,scl',ClCnt)
end
acr=sum(Prd==ClTest)/n_test;
