%{
given n discrete functions y(j,:), 1<=j<=n
corresponding to possibly different different x-value-sets x(i,:)
computes the sum of these reltive to a uniform scale of width h
%}
%first run 'Repeat_Classification'
addpath('../useful_functions');
switch Classifier
    case 1
        ClassifierName='k-near-neighb';
    case 2
        ClassifierName='svm';
    case 3
        ClassifierName='combined algorithm';
end
TitleName=strcat('average ROC-curve for',{' '},ClassifierName);
h=.01;
%all the Ys live on one scale
[X_uni,Y_uni]=multscale_to_onescale(X,Y,h);
nvecs=size(Y_uni,1);
n_entries=length(X_uni);
for i=1:n_entries
    ind1=~isnan(Y_uni(:,i));
    tmp1=Y_uni(ind1,i);
    Y_mean(i)=mean(tmp1);
end
figure;plot(X_uni,Y_uni);
hold on
plot(X_uni,Y_mean,'*'); grid on;
title(TitleName);
