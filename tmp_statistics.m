7oct

observation: pattern-NN performs better than NN

combined algorithm with NN, num_feat=30, c_pos=.7 (weight for TPR) 
neurons=[3,3] 
trainset nr.=1, acr=0.919,acr_w=0.816, TPR=0.765, TNR=0.936
 trainset nr.=2, acr=0.914,acr_w=0.846, TPR=0.813, TNR=0.924
 trainset nr.=3, acr=0.919,acr_w=0.881, TPR=0.862, TNR=0.925
 trainset nr.=4, acr=0.939,acr_w=0.798, TPR=0.725, TNR=0.967
 trainset nr.=5, acr=0.942,acr_w=0.894, TPR=0.871, TNR=0.949
 trainset nr.=6, acr=0.937,acr_w=0.894, TPR=0.872, TNR=0.945
 trainset nr.=7, acr=0.905,acr_w=0.843, TPR=0.813, TNR=0.914
 trainset nr.=8, acr=0.939,acr_w=0.868, TPR=0.833, TNR=0.950
 trainset nr.=9, acr=0.908,acr_w=0.770, TPR=0.698, TNR=0.938
 trainset nr.=10, acr=0.925,acr_w=0.849, TPR=0.811, TNR=0.939
  acr_mean=0.925,acr_w_mean=0.846, TPR_mean=0.806, TNR_mean=0.939

experiment: tried combined alorithm with pattern-NN of 5,10,20 neurons in hidden layer.
10 is the best

observation: combined algorithm with pattern-NN improves the performance
of the input classifiers

observation: knn performs better than svm on the weighted accuracy

combined algorithm with pattern-NN, num_feat=30,
neurons=10    
trainset nr.=1, acr=0.922,acr_w=0.876, TPR=0.853, TNR=0.930
 trainset nr.=2, acr=0.902,acr_w=0.863, TPR=0.844, TNR=0.908
 trainset nr.=3, acr=0.902,acr_w=0.898, TPR=0.897, TNR=0.903
 trainset nr.=4, acr=0.934,acr_w=0.845, TPR=0.800, TNR=0.951
 trainset nr.=5, acr=0.908,acr_w=0.948, TPR=0.968, TNR=0.902
 trainset nr.=6, acr=0.784,acr_w=0.927, TPR=1.000, TNR=0.756
 trainset nr.=7, acr=0.867,acr_w=0.831, TPR=0.813, TNR=0.873
 trainset nr.=8, acr=0.948,acr_w=0.916, TPR=0.900, TNR=0.953
 trainset nr.=9, acr=0.890,acr_w=0.901, TPR=0.907, TNR=0.888
 trainset nr.=10, acr=0.928,acr_w=0.940, TPR=0.946, TNR=0.926
  acr_mean=0.899,acr_w_mean=0.895, TPR_mean=0.893, TNR_mean=0.899
svm
 trainset nr.=1, acr=0.939,acr_w=0.803, TPR=0.735, TNR=0.962
 trainset nr.=2, acr=0.937,acr_w=0.833, TPR=0.781, TNR=0.952
 trainset nr.=3, acr=0.937,acr_w=0.863, TPR=0.828, TNR=0.947
 trainset nr.=4, acr=0.939,acr_w=0.765, TPR=0.675, TNR=0.974
 trainset nr.=5, acr=0.948,acr_w=0.853, TPR=0.806, TNR=0.962
 trainset nr.=6, acr=0.954,acr_w=0.849, TPR=0.795, TNR=0.974
 trainset nr.=7, acr=0.954,acr_w=0.755, TPR=0.656, TNR=0.984
 trainset nr.=8, acr=0.951,acr_w=0.782, TPR=0.700, TNR=0.975
 trainset nr.=9, acr=0.960,acr_w=0.864, TPR=0.814, TNR=0.980
 trainset nr.=10, acr=0.951,acr_w=0.840, TPR=0.784, TNR=0.971
  acr_mean=0.947,acr_w_mean=0.821, TPR_mean=0.757, TNR_mean=0.968
knn
 trainset nr.=1, acr=0.879,acr_w=0.842, TPR=0.824, TNR=0.885
 trainset nr.=2, acr=0.873,acr_w=0.895, TPR=0.906, TNR=0.870
 trainset nr.=3, acr=0.888,acr_w=0.847, TPR=0.828, TNR=0.893
 trainset nr.=4, acr=0.902,acr_w=0.851, TPR=0.825, TNR=0.912
 trainset nr.=5, acr=0.908,acr_w=0.926, TPR=0.935, TNR=0.905
 trainset nr.=6, acr=0.937,acr_w=0.860, TPR=0.821, TNR=0.951
 trainset nr.=7, acr=0.867,acr_w=0.789, TPR=0.750, TNR=0.879
 trainset nr.=8, acr=0.896,acr_w=0.787, TPR=0.733, TNR=0.912
 trainset nr.=9, acr=0.914,acr_w=0.848, TPR=0.814, TNR=0.928
 trainset nr.=10, acr=0.905,acr_w=0.860, TPR=0.838, TNR=0.913
  acr_mean=0.897,acr_w_mean=0.851, TPR_mean=0.827, TNR_mean=0.905

---------------------------------------------
6oct

observation: standard neural net seems to outperform pattern-neural-net
 
combined algorithm with pattern-NN num_feat=20, c_pos=.5
 trainset nr.=1, acr=0.908,acr_w=0.910, TPR=0.912, TNR=0.907
 trainset nr.=2, acr=0.092,acr_w=0.500, TPR=1.000, TNR=0.000
 trainset nr.=3, acr=0.905,acr_w=0.901, TPR=0.897, TNR=0.906
 trainset nr.=4, acr=0.934,acr_w=0.854, TPR=0.750, TNR=0.958
 trainset nr.=5, acr=0.916,acr_w=0.925, TPR=0.935, TNR=0.915
 trainset nr.=6, acr=0.937,acr_w=0.942, TPR=0.949, TNR=0.935
 trainset nr.=7, acr=0.879,acr_w=0.835, TPR=0.781, TNR=0.889
 trainset nr.=8, acr=0.951,acr_w=0.883, TPR=0.800, TNR=0.965
 trainset nr.=9, acr=0.879,acr_w=0.901, TPR=0.930, TNR=0.872
 trainset nr.=10, acr=0.919,acr_w=0.931, TPR=0.946, TNR=0.916
  acr_mean=0.832,acr_w_mean=0.858, TPR_mean=0.890, TNR_mean=0.826

observation: going from 30 to 60 features acr_w increaes in the combined algorithm
decreases in svm and increases in knn

combined algorithm with standard NN num_feat=60
 trainset nr.=1, acr=0.942,acr_w=0.850, TPR=0.735, TNR=0.965
 trainset nr.=2, acr=0.925,acr_w=0.889, TPR=0.844, TNR=0.933
 trainset nr.=3, acr=0.899,acr_w=0.898, TPR=0.897, TNR=0.899
 trainset nr.=4, acr=0.945,acr_w=0.849, TPR=0.725, TNR=0.974
 trainset nr.=5, acr=0.954,acr_w=0.975, TPR=1.000, TNR=0.949
 trainset nr.=6, acr=0.937,acr_w=0.919, TPR=0.897, TNR=0.942
 trainset nr.=7, acr=0.931,acr_w=0.836, TPR=0.719, TNR=0.952
 trainset nr.=8, acr=0.888,acr_w=0.878, TPR=0.867, TNR=0.890
 trainset nr.=9, acr=0.914,acr_w=0.871, TPR=0.814, TNR=0.928
 trainset nr.=10, acr=0.925,acr_w=0.910, TPR=0.892, TNR=0.929
  acr_mean=0.926,acr_w_mean=0.888, TPR_mean=0.839, TNR_mean=0.936
 statistics for first level classifiers:
svm
 trainset nr.=1, acr=0.954,acr_w=0.843, TPR=0.706, TNR=0.981
 trainset nr.=2, acr=0.951,acr_w=0.819, TPR=0.656, TNR=0.981
 trainset nr.=3, acr=0.942,acr_w=0.796, TPR=0.621, TNR=0.972
 trainset nr.=4, acr=0.934,acr_w=0.745, TPR=0.500, TNR=0.990
 trainset nr.=5, acr=0.954,acr_w=0.887, TPR=0.806, TNR=0.968
 trainset nr.=6, acr=0.948,acr_w=0.814, TPR=0.641, TNR=0.987
 trainset nr.=7, acr=0.948,acr_w=0.747, TPR=0.500, TNR=0.994
 trainset nr.=8, acr=0.957,acr_w=0.780, TPR=0.567, TNR=0.994
 trainset nr.=9, acr=0.948,acr_w=0.811, TPR=0.628, TNR=0.993
 trainset nr.=10, acr=0.951,acr_w=0.770, TPR=0.541, TNR=1.000
  acr_mean=0.949,acr_w_mean=0.801, TPR_mean=0.617, TNR_mean=0.986
knn
 trainset nr.=1, acr=0.836,acr_w=0.857, TPR=0.882, TNR=0.831
 trainset nr.=2, acr=0.865,acr_w=0.897, TPR=0.938, TNR=0.857
 trainset nr.=3, acr=0.859,acr_w=0.876, TPR=0.897, TNR=0.855
 trainset nr.=4, acr=0.885,acr_w=0.859, TPR=0.825, TNR=0.893
 trainset nr.=5, acr=0.865,acr_w=0.897, TPR=0.935, TNR=0.858
 trainset nr.=6, acr=0.899,acr_w=0.898, TPR=0.897, TNR=0.899
 trainset nr.=7, acr=0.867,acr_w=0.871, TPR=0.875, TNR=0.867
 trainset nr.=8, acr=0.890,acr_w=0.880, TPR=0.867, TNR=0.893
 trainset nr.=9, acr=0.890,acr_w=0.878, TPR=0.860, TNR=0.895
 trainset nr.=10, acr=0.879,acr_w=0.885, TPR=0.892, TNR=0.877
  acr_mean=0.873,acr_w_mean=0.880, TPR_mean=0.887, TNR_mean=0.872
---------------------------------------------
combined algorithm, numfeat=30 
trainset nr.=1, acr=0.893,acr_w=0.836, TPR=0.765, TNR=0.907
 trainset nr.=2, acr=0.945,acr_w=0.886, TPR=0.813, TNR=0.959
 trainset nr.=3, acr=0.928,acr_w=0.898, TPR=0.862, TNR=0.934
 trainset nr.=4, acr=0.939,acr_w=0.846, TPR=0.725, TNR=0.967
 trainset nr.=5, acr=0.934,acr_w=0.891, TPR=0.839, TNR=0.943
 trainset nr.=6, acr=0.945,acr_w=0.913, TPR=0.872, TNR=0.955
 trainset nr.=7, acr=0.919,acr_w=0.787, TPR=0.625, TNR=0.949
 trainset nr.=8, acr=0.931,acr_w=0.902, TPR=0.867, TNR=0.937
 trainset nr.=9, acr=0.937,acr_w=0.824, TPR=0.674, TNR=0.974
 trainset nr.=10, acr=0.925,acr_w=0.922, TPR=0.919, TNR=0.926
  acr_mean=0.930,acr_w_mean=0.871, TPR_mean=0.796, TNR_mean=0.945
 statistics for first level classifiers:
svm
 trainset nr.=1, acr=0.939,acr_w=0.848, TPR=0.735, TNR=0.962
 trainset nr.=2, acr=0.937,acr_w=0.867, TPR=0.781, TNR=0.952
 trainset nr.=3, acr=0.937,acr_w=0.887, TPR=0.828, TNR=0.947
 trainset nr.=4, acr=0.939,acr_w=0.824, TPR=0.675, TNR=0.974
 trainset nr.=5, acr=0.948,acr_w=0.884, TPR=0.806, TNR=0.962
 trainset nr.=6, acr=0.954,acr_w=0.884, TPR=0.795, TNR=0.974
 trainset nr.=7, acr=0.954,acr_w=0.820, TPR=0.656, TNR=0.984
 trainset nr.=8, acr=0.951,acr_w=0.837, TPR=0.700, TNR=0.975
 trainset nr.=9, acr=0.960,acr_w=0.887, TPR=0.791, TNR=0.984
 trainset nr.=10, acr=0.951,acr_w=0.877, TPR=0.784, TNR=0.971
  acr_mean=0.947,acr_w_mean=0.862, TPR_mean=0.755, TNR_mean=0.968
knn
 trainset nr.=1, acr=0.879,acr_w=0.854, TPR=0.824, TNR=0.885
 trainset nr.=2, acr=0.873,acr_w=0.888, TPR=0.906, TNR=0.870
 trainset nr.=3, acr=0.888,acr_w=0.860, TPR=0.828, TNR=0.893
 trainset nr.=4, acr=0.902,acr_w=0.869, TPR=0.825, TNR=0.912
 trainset nr.=5, acr=0.908,acr_w=0.920, TPR=0.935, TNR=0.905
 trainset nr.=6, acr=0.937,acr_w=0.886, TPR=0.821, TNR=0.951
 trainset nr.=7, acr=0.867,acr_w=0.815, TPR=0.750, TNR=0.879
 trainset nr.=8, acr=0.896,acr_w=0.823, TPR=0.733, TNR=0.912
 trainset nr.=9, acr=0.914,acr_w=0.871, TPR=0.814, TNR=0.928
 trainset nr.=10, acr=0.905,acr_w=0.875, TPR=0.838, TNR=0.913
  acr_mean=0.897,acr_w_mean=0.866, TPR_mean=0.827, TNR_mean=0.905>> 

------------------------------------
27sep 

nfeat=30 
trainset nr.=1, acr=0.911, TPR=0.794, PPV=0.529
 trainset nr.=2, acr=0.925, TPR=0.912, PPV=0.574
 trainset nr.=3, acr=0.905, TPR=0.794, PPV=0.509
 trainset nr.=4, acr=0.905, TPR=0.794, PPV=0.509
 trainset nr.=5, acr=0.908, TPR=0.882, PPV=0.517
 trainset nr.=6, acr=0.899, TPR=0.706, PPV=0.490
 trainset nr.=7, acr=0.914, TPR=0.912, PPV=0.534
 trainset nr.=8, acr=0.914, TPR=0.794, PPV=0.540
 trainset nr.=9, acr=0.919, TPR=0.794, PPV=0.563
 trainset nr.=10, acr=0.902, TPR=0.765, PPV=0.500
  acr_mean=0.910, TPR_mean=0.815, PPV_mean=0.527

combined algorithm, inputs: svm,knn, nfeat=20
trainset nr.=1, acr=0.919, TPR=0.824, PPV=0.560
 trainset nr.=2, acr=0.937, TPR=0.676, PPV=0.676
 trainset nr.=3, acr=0.914, TPR=0.853, PPV=0.537
 trainset nr.=4, acr=0.931, TPR=0.794, PPV=0.614
 trainset nr.=5, acr=0.934, TPR=0.676, PPV=0.657
 trainset nr.=6, acr=0.922, TPR=0.824, PPV=0.571
 trainset nr.=7, acr=0.937, TPR=0.676, PPV=0.676
 trainset nr.=8, acr=0.931, TPR=0.676, PPV=0.639
 trainset nr.=9, acr=0.934, TPR=0.676, PPV=0.657
 trainset nr.=10, acr=0.922, TPR=0.794, PPV=0.574
  acr_mean=0.928, TPR_mean=0.747, PPV_mean=0.616>> 
-----------------------------------------
20sep

applied svm with crossvalidation to unbalanced trainset. 
now produces reasonable results.

encountered interesting effect. svm parameters chosen by 
crossvalidation give excellent result on validation set
but poor result on testset (acr~.9, tpr~.03). reconstructed old
parameters which where computed by crossvalidation also.
why are the new parameters different from the old?

knearneig, numneig:80, numfeat:40, gaussian weights
xmax=10 (width of gaussian (parameter in 'classifyknearneig.m')) 
trainset nr.=1, acr=0.911, TPR=0.618, PPV=0.538
 trainset nr.=2, acr=0.934, TPR=0.688, PPV=0.629
 trainset nr.=3, acr=0.931, TPR=0.621, PPV=0.581
 trainset nr.=4, acr=0.916, TPR=0.575, PPV=0.657
 trainset nr.=5, acr=0.942, TPR=0.839, PPV=0.634
 trainset nr.=6, acr=0.951, TPR=0.667, PPV=0.867
 trainset nr.=7, acr=0.931, TPR=0.594, PPV=0.633
 trainset nr.=8, acr=0.948, TPR=0.700, PPV=0.700
 trainset nr.=9, acr=0.925, TPR=0.674, PPV=0.707
 trainset nr.=10, acr=0.954, TPR=0.703, PPV=0.839
  acr_mean=0.934, TPR_mean=0.668, PPV_mean=0.678>>

knearneig, numneig:80, numfeat:40, xmax=3
trainset nr.=1, acr=0.879, TPR=0.824, PPV=0.438
 trainset nr.=2, acr=0.905, TPR=0.906, PPV=0.492
 trainset nr.=3, acr=0.916, TPR=0.828, PPV=0.500
 trainset nr.=4, acr=0.914, TPR=0.750, PPV=0.600
 trainset nr.=5, acr=0.908, TPR=0.935, PPV=0.492
 trainset nr.=6, acr=0.937, TPR=0.846, PPV=0.673
 trainset nr.=7, acr=0.908, TPR=0.813, PPV=0.500
 trainset nr.=8, acr=0.916, TPR=0.800, PPV=0.511
 trainset nr.=9, acr=0.905, TPR=0.791, PPV=0.586
 trainset nr.=10, acr=0.931, TPR=0.838, PPV=0.633
  acr_mean=0.912, TPR_mean=0.833, PPV_mean=0.542
  
experimented knearneig with different num of feat:
10,20,40,50,60. the best results are 40

knearneig, numneig:80, numfeat:40, equal weights
trainset nr.=1, acr=0.870, TPR=0.912, PPV=0.425
 trainset nr.=2, acr=0.867, TPR=0.906, PPV=0.403
 trainset nr.=3, acr=0.865, TPR=0.897, PPV=0.371
 trainset nr.=4, acr=0.890, TPR=0.850, PPV=0.515
 trainset nr.=5, acr=0.873, TPR=0.935, PPV=0.408
 trainset nr.=6, acr=0.902, TPR=0.846, PPV=0.541
 trainset nr.=7, acr=0.847, TPR=0.750, PPV=0.348
 trainset nr.=8, acr=0.902, TPR=0.833, PPV=0.463
 trainset nr.=9, acr=0.888, TPR=0.860, PPV=0.529
 trainset nr.=10, acr=0.890, TPR=0.946, PPV=0.493
  acr_mean=0.880, TPR_mean=0.874, PPV_mean=0.450

svm,num_feat=20 
trainset nr.=1, acr=0.939, TPR=0.794, PPV=0.659
 trainset nr.=2, acr=0.925, TPR=0.781, PPV=0.568
 trainset nr.=3, acr=0.922, TPR=0.793, PPV=0.523
 trainset nr.=4, acr=0.928, TPR=0.700, PPV=0.683
 trainset nr.=5, acr=0.934, TPR=0.839, PPV=0.591
 trainset nr.=6, acr=0.948, TPR=0.846, PPV=0.733
 trainset nr.=7, acr=0.942, TPR=0.656, PPV=0.700
 trainset nr.=8, acr=0.942, TPR=0.733, PPV=0.647
 trainset nr.=9, acr=0.942, TPR=0.791, PPV=0.756
 trainset nr.=10, acr=0.951, TPR=0.811, PPV=0.750
  acr_mean=0.937, TPR_mean=0.774, PPV_mean=0.661>> 

  experimented svm with different num of feat:
10,20,30,40,60. the best results are 30
svm,num_feat=30
 
trainset nr.=1, acr=0.942, TPR=0.706, PPV=0.706
 trainset nr.=2, acr=0.942, TPR=0.781, PPV=0.658
 trainset nr.=3, acr=0.945, TPR=0.759, PPV=0.647
 trainset nr.=4, acr=0.934, TPR=0.625, PPV=0.758
 trainset nr.=5, acr=0.954, TPR=0.806, PPV=0.714
 trainset nr.=6, acr=0.954, TPR=0.744, PPV=0.829
 trainset nr.=7, acr=0.954, TPR=0.656, PPV=0.808
 trainset nr.=8, acr=0.948, TPR=0.633, PPV=0.731
 trainset nr.=9, acr=0.963, TPR=0.767, PPV=0.917
 trainset nr.=10, acr=0.960, TPR=0.784, PPV=0.829
 acr_mean=0.950, TPR_mean=0.726, PPV_mean=0.759
------------------------------
16sep

 num_neighb=80 
trh-knn
trainset nr.=1, acr=0.920, TPR=0.923, PPV=0.462
 trainset nr.=2, acr=0.936, TPR=0.829, PPV=0.617
 trainset nr.=3, acr=0.912, TPR=0.842, PPV=0.542
 trainset nr.=4, acr=0.933, TPR=0.851, PPV=0.690
 trainset nr.=5, acr=0.947, TPR=0.884, PPV=0.717
 trainset nr.=6, acr=0.899, TPR=0.844, PPV=0.551
 trainset nr.=7, acr=0.923, TPR=0.919, PPV=0.567
 trainset nr.=8, acr=0.949, TPR=0.837, PPV=0.750
 trainset nr.=9, acr=0.928, TPR=0.794, PPV=0.574
 trainset nr.=10, acr=0.925, TPR=0.735, PPV=0.568>> Repeat_Classification

 knn
 trainset nr.=1, acr=0.875, TPR=0.951, PPV=0.464
 trainset nr.=2, acr=0.867, TPR=0.857, PPV=0.450
 trainset nr.=3, acr=0.856, TPR=0.846, PPV=0.407
 trainset nr.=4, acr=0.861, TPR=0.829, PPV=0.430
 trainset nr.=5, acr=0.848, TPR=0.875, PPV=0.402
 trainset nr.=6, acr=0.899, TPR=0.971, PPV=0.479
 trainset nr.=7, acr=0.893, TPR=0.921, PPV=0.486
 trainset nr.=8, acr=0.880, TPR=0.949, PPV=0.463
 trainset nr.=9, acr=0.907, TPR=0.957, PPV=0.577
 trainset nr.=10, acr=0.867, TPR=0.957, PPV=0.478>>
-------------------------------
8sep, balanced trainset

svm, num_feat=20 
accuracy=0.933, TPR=0.833, PPV=0.702 
accuracy=0.931, TPR=0.884, PPV=0.644 
accuracy=0.939, TPR=0.854, PPV=0.673 

knn:
num_feat=60 
 num_neighb=80
 accuracy=0.892, TPR=0.933, PPV=0.5
 accuracy=0.921, TPR=0.947, PPV=0.563 
 accuracy=0.865, TPR=0.932, PPV=0.415 

 num_neighb=160
 accuracy=0.873, TPR=0.916, PPV=0.463 
 accuracy=0.888, TPR=0.800, PPV=0.504 
 accuracy=0.862, TPR=0.875, PPV=0.401 
-------------------------------------
7sep, balanced trainset

neural net,  num_feat=20
accuracy=0.872, TPR=0.854, PPV=0.418 
accuracy=0.918, TPR=0.707, PPV=0.631
accuracy=0.920, TPR=0.794, PPV=0.649 

knn: num_feat=20,num_neighb=20, equal weights 
 accuracy=0.872, TPR=0.908, PPV=0.504
 accuracy=0.874, TPR=0.868, PPV=0.451
 accuracy=0.866, TPR=0.873, PPV=0.482 



------------------------------------------
31aug,  balanced trainset, trainAFratio=.5, num_feat=20, random trainsets

svm, unbalanced trainset 
accuracy=0.911, ppv=0.600, npv=0.984 
accuracy=0.902, ppv=0.575, npv=0.969 
accuracy=0.906, ppv=0.472, npv=0.985 

svm, balanced trainset
accuracy=0.858, ppv=0.716, npv=0.892 
accuracy=0.856, ppv=0.788, npv=0.867
accuracy=0.844, ppv=0.778, npv=0.859 

knn: num_neighb=20, equal weights
accuracy=0.742, ppv=0.717, npv=0.747 
accuracy=0.773, ppv=0.761, npv=0.776
accuracy=0.773, ppv=0.761, npv=0.776 

knn: num_neighb=20, gaussian weights
accuracy=0.850, ppv=0.629, npv=0.897
accuracy=0.870, ppv=0.789, npv=0.885 
accuracy=0.841, ppv=0.655, npv=0.878 

knn: num_neighb=50
accuracy=0.856, ppv=0.618, npv=0.912 
accuracy=0.799, ppv=0.772, npv=0.804 
accuracy=0.822, ppv=0.732, npv=0.838 

nnet with knn
accuracy=0.901, ppv=0.569, npv=0.966
accuracy=0.884, ppv=0.446, npv=0.983 
accuracy=0.864, ppv=0.493, npv=0.954 

nnet with svm
accuracy=0.878, ppv=0.807, npv=0.892
accuracy=0.861, ppv=0.673, npv=0.894 
accuracy=0.785, ppv=0.863, npv=0.772 
 
nnet with knn+svm
accuracy=0.873, ppv=0.500, npv=0.946 
accuracy=0.875, ppv=0.446, npv=0.956 
accuracy=0.875, ppv=0.576, npv=0.935 

-------------------------------------
nnet with processed features, balanced trainset, num_feat=20,ntrain=.7, random trainsets

svm+knn
 accuracy=0.991501, ppv=0.969231, npv=0.996528
 accuracy=0.997167, ppv=0.981818, npv=1 
 accuracy=0.991501, ppv=0.948276, npv=1
 accuracy=0.983003, ppv=0.903226, npv=1 
 accuracy=1, ppv=1, npv=1
 
 nnet with knn
 accuracy=0.997167, ppv=0.984375, npv=1 
 accuracy=0.997167, ppv=1, npv=0.996564 
 accuracy=0.994334, ppv=0.983333, npv=0.996587
  accuracy=0.991501, ppv=0.952381, npv=1 
 accuracy=0.985836, ppv=0.915254, npv=1 

 knn, no duplicates in trainset
 accuracy=0.994, ppv=0.963, npv=1.000
  accuracy=0.994, ppv=0.963, npv=1.000 
 accuracy=0.997, ppv=1.000, npv=0.997
  accuracy=0.997, ppv=1.000, npv=0.997 
 accuracy=1.000, ppv=1.000, npv=1.000 



-----------------------------------------

num_neighb=10
 num_feat=20
 accuracy=0.868085, ppv=0.837838, npv=0.873737 

num_neighb=15
 num_feat=20
 accuracy=0.876596, ppv=0.837838, npv=0.883838 

num_neighb=20
 num_feat=20
 accuracy=0.880851, ppv=0.864865, npv=0.883838 
 
  num_neighb=30
 num_feat=20
 accuracy=0.885106, ppv=0.918919, npv=0.87878
 
  num_neighb=40
 num_feat=20
 accuracy=0.889362, ppv=0.972973, npv=0.873737
 
  num_neighb=60
 num_feat=20
 accuracy=0.880851, ppv=0.972973, npv=0.863636
------------------------------------
nnet (1 input: output of svm) balanced set
  num_feat=7
 my_accuracy=0.625532, ppv=0.918919, npv=0.570707
 
 num_feat=20
 accuracy=0.897872, ppv=0.945946, npv=0.888889 
---------------------------------------------------
svm balanced set

 num_feat=7
 accuracy=0.829787, ppv=0.918919, npv=0.813131
 
 num_feat=14
 accuracy=0.838298, ppv=0.918919, npv=0.823232 
 
  num_feat=20
 accuracy=0.876596, ppv=0.945946, npv=0.863636 
 
  num_feat=27
 accuracy=0.893617, ppv=0.945946, npv=0.883838
  
 ------------------------------------------------
