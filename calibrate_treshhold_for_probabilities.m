%{
calibrates treshhold for treshholded knn method. in the ordinary
knn method the treshhold is .5 which means that if a sample
has a probability of >= .5 of being positive it is classified as positive.
however another treshhold might improve the performance of knn and this
is the idea of the treshholded knn classifier.
%}

%acr=calc_acr_on_probabilities_with_trh(Prob,Cl,trh);
clear all
LoadData=1;
nts=10; % num of random trainsets
for i=1:nts
    if (LoadData)
        load('partitions');
        set_loadparams;
        opts.new_perm_FLAG=3;
        [Feat,Cl,ClProb,NCl,IndTrain,IndConf, ...
            IndTest,DataStat]=Load_and_Separate(DataFlag,opts,partition(i));
        nFeatAct=20; % num of active features
        LoadFeatRank=1; % load featurerank
        if LoadFeatRank
            switch DataFlag
                case 1
                    load('FeatRank_sep15');
                case 2
                    load('FeatRank_jul15');
            end
        else
            FeatSelMeth=2; %1:shannon, 2:mrmr
            FeatRank=Feature_Selection(Feat(:,IndTrain)',Cl(IndTrain),FeatSelMeth);
        end
        FeatAct=FeatRank(1:nFeatAct);
        Feat=Feat(FeatAct,:);
    end
    nNeig=40; % num of neighbours
    dummy=0;
    opts.weight=2; % weights: 1-equal, 2-gaussian dist
    opts.conf=2; %confidence type 1-correct, 2-positive
    %calc confidence of sample beeing true
    ConfP=ClassifyKNearNeig(Feat(:,IndConf),Feat(:,IndTrain),dummy,Cl(IndTrain),nNeig,opts);
    myfun=@(trh) calc_acr_on_probabilities_with_trh(ConfP(1,:)',Cl(IndConf),trh);
    trh0=.5;
    tmp=myfun(trh0);
    h=.01;
    trhs=0:h:1; % tresholds for knn
    for k=1:length(trhs)
        acr_w(k)=myfun(trhs(k));
    end
    [acr_w_max(i),indmax]=max(acr_w);
    trh_opt(i)=trhs(indmax);
    %trh_opt=fminsearch(myfun,trh0);
    %acr_opt=1-myfun(trh_opt);
end
