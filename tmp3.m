myfun = @(trh) 1-calc_acr_on_probabilities_with_trh(Prob,Cl,trh);

trh=[0:.01:1]; 
y=[];
for i=1:length(trh)
    y(i)=myfun(trh(i));
end
figure;
plot(trh,y); grid on;