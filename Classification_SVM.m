% classification using libsvm
addpath(genpath('.'));
addpath('../useful_functions');
load('partitions');
opts=set_loadparams();
opts.nFeatAct=2;
partition_in=partitions(1);
[Feat,Cl,ClProb,DataStat,partition_out]=Load_data(partition_in,opts);
opts1=[];
[Prd,Scores]=Classification_SVM_fun(Feat,Cl,partition_out,opts1);
if Prd(1)~=round(Scores(1))
    Scores=1-Scores;
end
%compute statistics
arg1.name='trh_FP';
arg1.value=.1;
arg1.scores=Scores;
stats=comp_stat(Prd,Cl(partition_out.IndTest),arg1);
opts2.A_trh=1;
print_statistics(stats,opts2);
[X1,Y1]=perfcurve(Cl(partition_out.IndTest),Scores,1);
if Prd(1)~=round(Scores(1))
    tmp=X1;
    X1=Y1;
    Y1=tmp;
end
plot(X1,Y1,'*');
