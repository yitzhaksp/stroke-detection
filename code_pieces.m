piece 1
    if (Classifier==3)
        if (PrintAF)
            n_AF=2; % num of artificial features
            fprintf('\n statistics for first level classifiers:');
            for i=1:n_AF
                fprintf('\n%s',stats_AF{1}(i).Classifier);
                acr_AF_mean=0;
                acr_w_AF_mean=0;
                TPR_AF_mean=0;
                TNR_AF_mean=0;
                for j=1:nts
                    %fprintf('\n trainset nr.=%d,acr_w=%.3f, TPR=%.3f, TNR=%.3f',j,stats_AF{j}(i).acr_ind,stats_AF{j}(i).acr_w_ind, stats_AF{j}(i).TPR_ind, stats_AF{j}(i).TNR_ind);
                    acr_AF_mean=acr_AF_mean+stats_AF{j}(i).acr_ind;
                    acr_w_AF_mean=acr_w_AF_mean+stats_AF{j}(i).acr_w_ind;
                    TPR_AF_mean=TPR_AF_mean+stats_AF{j}(i).TPR_ind;
                    TNR_AF_mean=TNR_AF_mean+stats_AF{j}(i).TNR_ind;
                end
                acr_AF_mean=acr_AF_mean/nts;
                acr_w_AF_mean=acr_w_AF_mean/nts;
                TPR_AF_mean=TPR_AF_mean/nts;
                TNR_AF_mean=TNR_AF_mean/nts;
                fprintf('\n  acr_w_mean=%.3f, TPR_mean=%.3f, TNR_mean=%.3f',acr_w_AF_mean,TPR_AF_mean,TNR_AF_mean);
            end
        end
    end