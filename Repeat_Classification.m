addpath(genpath('.'));
addpath('../useful_functions');
clear all
load('partitions');
nts=10; % num of random trainsets (must be <=100 )
Classifier=1; % 1:knn, 2:svm, 3:NN, 4:adaboost
opts=set_loadparams();
%loop goes over various numbers of features
nums_feat=61;
% parameters for roc-curve
arg1.name='trh_FP';
arg1.value=.2;
% does the classifier also return scores? (this is just the default value)
ProbFlag=1;
ROC=1; % calc roc-curve
switch Classifier
    case 1
        opts1.nNeig=40;
        opts1.trh_Flag=2; % use treshold? 0:no, 1: opts.trh_cls, 2:calibrate trh
        opts1.trh_cls=.4;
        opts1.print=1;
    case 2
        opts1=[];
    case 3
        opts1.MLFeatFlag=1;
        %which processed features shall be computed?
        opts1.MLFeatInputs.svm=1;
        opts1.MLFeatInputs.knn=1;
        ProbFlag=0;
        PrintAF=0;
    case 4
        opts1.n_trees=100;
        ProbFlag=0;                
end
for i=1:length(nums_feat)
    opts.nFeatAct=nums_feat(i);
    for k=1:nts
        [Feat,Cl,ClProb,DataStat,partition_out]=Load_data(partitions(k),opts);
        switch Classifier
            case 1
                [Prd,arg1.scores]=Classification_KNearNeig_fun(Feat,Cl,partition_out,opts1);
            case 2
                [Prd,arg1.scores]=Classification_SVM_fun(Feat,Cl,partition_out,opts1);
                % we want the score to be the probability of the positive
                % class
                if Prd(1)~=round(arg1.scores(1))
                    arg1.scores=1-arg1.scores;
                end
            case 3
                [Prd,arg1.scores]=Classification_NN_fun(Feat,Cl,ClProb,DataStat.NCl,partition_out,opts1);
                %Prd=Classification_patternNN_fun(Feat,Cl,ClProb,partition_out,opts1);
                Prd=Prd';
            case 4
                Mdl = fitensemble(Feat(:,partition_out.IndTrain)',Cl(partition_out.IndTrain),'LogitBoost',opts1.n_trees,'Tree');
                Prd = predict(Mdl,Feat(:,partition_out.IndTest)');
                
        end
        %calc roc-curve
        if(ROC && ProbFlag)
            [X{k},Y{k}]=perfcurve(Cl(partition_out.IndTest),arg1.scores,1);
        end
        if ProbFlag
            stats{k}=comp_stat(Prd,Cl(partition_out.IndTest),arg1);
        else
            stats{k}=comp_stat(Prd,Cl(partition_out.IndTest));
        end
        acr_w(k)=stats{k}.acr_w;
        TPR(k)=stats{k}.TPR;
        TNR(k)=stats{k}.TNR;
        if ProbFlag
            A_trh_ratio(k)=stats{k}.A_trh_ratio;
        end
    end
    if (ROC && ProbFlag)
    end
    
    %print statistics
    opts2.A_trh=1;
    for k=1:nts
        %print_statistics(stats{k},opts2);
    end
    stats_mean.acr_w=mean(acr_w);
    stats_mean.TPR=mean(TPR);
    stats_mean.TNR=mean(TNR);
    if ProbFlag
        stats_mean.A_trh_ratio=mean(A_trh_ratio);
    end
    fprintf('\n average statistics for num_feat=%d',opts.nFeatAct);
    print_statistics(stats_mean,opts2); fprintf('\n');
    % print statistics of first level classifier in the case
    %we are using combined algorithm (code pieces 3)
end