LoadData=0;
if (LoadData)
    Load_and_Separate;
    Feature_Selection;
end
PlotStat=1; 
PlotConf=0;

gamma=1.0; % width of gaussian
Conf=ClassifyRBF(Feat(:,IndTest),Feat(:,IndTrain),Cl(IndTrain),gamma);
[maxx,Prd]=max(Conf);
Prd=Prd';

%compute confidence of correct classification
nNeig=5;
opts.weight=2; % weights: 1-equal, 2-gaussian dist
opts.conf=1; %confidence type 1-correct, 2-positive 
opts.gamma=1.0; % width of gaussian
ConfC=ClassifyKNearNeig(Feat(:,IndTest),Feat(:,IndTrain),dummy,Cl(IndTrain),nNeig,opts);
cnf=comp_stat_cnf(Cl(IndTest),ConfC);

%compute statistics
h=.1;
trh=[0:h:1];
stats=comp_stat1(Prd,Cl(IndTest),ConfC,trh,ClMainDense);
if (PlotStat)
    subplot(3,1,1), grid on, plot(trh,stats.acr,'*'); title('acr');grid on;
    subplot(3,1,2), plot(trh,stats.PPV,'*'); title('PPV'); grid on;
    subplot(3,1,3), plot(trh,stats.NPV,'*'); title('NPV');grid on;
end


if (PlotConf)
subplot(2,1,1), hist(cnf.ConfP);
subplot(2,1,2), hist(cnf.ConfN);
end