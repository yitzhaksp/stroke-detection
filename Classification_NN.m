addpath(genpath('.'));
addpath('../useful_functions');
load('partitions');
opts=set_loadparams();
opts.nFeatAct=45;
partition_in=partition(1);
[Feat,Cl,ClProb,DataStat,partition_out]=Load_data(partition_in,opts);
%use combined algorithm?
opts1.MLFeatFlag=1;
%which processed features shall be computed?
opts1.MLFeatInputs.svm=1;
opts1.MLFeatInputs.knn=1;
patternNNFlag=0;
if patternNNFlag
    Prd=Classification_patternNN_fun(Feat,Cl,ClProb,partition_out,opts1);
else
    [Prd,Scores]=Classification_NN_fun(Feat,Cl,ClProb,DataStat.NCl,partition_out,opts1);
end
arg1.name='trh_FP';
arg1.value=.1;
arg1.scores=Scores;
stats=comp_stat(Prd',Cl(partition_out.IndTest),arg1);
opts2.A_trh=1;
print_statistics(stats,opts2);
[X2,Y2]=perfcurve(Cl(partition_out.IndTest),Scores,1);
%figure;plot(X2,Y2);
