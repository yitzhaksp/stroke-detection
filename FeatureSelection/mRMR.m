%{
%minimal-Redundance-maximal-Relevance method for
%Feature selection.
 see: Bonev 'feature selection based on information theory' chpt 2.4.5

Features should be normalized
%e.g. in the range[0,100] since the width of a bin
in pdf estimation is equal to 1.0
%Features should be normalized

inputs: Featurematrix, Classesvector
%}
function FeatSelInd=mRMR(Feat,Cls)
nFeat=size(Feat,2);
for i=1:nFeat
    %Relevance
    Rel(i)=mutualinfo_mtlb(Feat(:,i),Cls);
end
[dummy,FeatSelInd]=max(Rel);
%
FeatSelComplInd=[1:nFeat];
FeatSelComplInd(FeatSelInd)=[];
i=1;
while (i<nFeat)
    %Red: redundancy
    [Rel,Red]=CompRelRed(Feat,Cls,FeatSelInd);
    RelRedDiff=Rel-Red;
    [dummy,Indtmp]=max(RelRedDiff);
    NextInd=FeatSelComplInd(Indtmp);
    FeatSelInd=[FeatSelInd,NextInd];
    FeatSelComplInd(Indtmp)=[];
    i=i+1;
end