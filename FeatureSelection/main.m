clear
clc

% load Boston housing data
load housing.txt

% convert it into a classification dataset (based on the terget mean)
% MAKE SURE SMALLER CLASS IS CLASS 1, for this dataset it is OK
[row col] = size(housing);
q1 = find(housing(:, col) >= mean(housing(:, col)));
q0 = find(housing(:, col) < mean(housing(:, col)));
sample = housing;
sample(q1, col) = 1;
sample(q0, col) = 0;

% define stopping criterion for feature selection
Eps = 0.0001;

% select best features using forward method
fprintf('\n\nDoing forward feature selection...');
[featuresFW snFW sn_stdFW spFW sp_stdFW] = feat_selFW(sample, Eps);

% select best features using backward method
fprintf('\n\nDoing backward feature selection...');
[featuresBW removed_featuresBW snBW sn_stdBW spBW sp_stdBW] = feat_selBW(sample, Eps);

% NOW COMPARE AND PRESENT RESULTS ON YOUR OWN
