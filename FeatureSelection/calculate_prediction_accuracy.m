function [sensitivity, sensitivity_std, specificity, specificity_std]  = calculate_prediction_accuracy (sample, features)

% THIS IS A SAMPLE FOR THIS FUNCTION
% MODIFY IT ACCORDING TO EACH PROBLEM SUCH THAT THE INPUTS/OUTPUTS ARE THE SAME
% FOR EXAMPLE, INSTEAD OF LOGISTIC REGRESSION USE NEURAL NETWORKS
% APPLY PCA IF NECESSARY
% ETC

% select features from the sample and add last column since this is the target variable
sample = sample(:, [features size(sample, 2)]);

% in 100 random selections estimate sensitivity and specificity of the predictor
% maybe 100 iterations is too many/few, consider changing it to some other value if necessary
for i = 1 : 100
    train = [];
    test = [];

    % find zero and one examples
    q0 = find(sample(:, size(sample, 2)) == 0);
    q1 = find(sample(:, size(sample, 2)) == 1);
    
    % randomly separate 10% from each into test set
    q0 = q0(randperm(length(q0)));
    q1 = q1(randperm(length(q1)));

    n0 = floor(0.1 * length(q0));
    test = [test; sample(q0(1 : n0), :)];
    n1 = floor(0.1 * length(q1));
    test = [test; sample(q1(1 : n1), :)];

    % make balanced training set
    train = sample(q1(n1 + 1 : length(q1)), :);
    train = [train; sample(q0(n1 + 1 : length(q1)), :)];
    
    % print out matrix dimensions and set some variables
    [n_examples, n_cols] = size(train);
    n_features = n_cols - 1;
    
    % normalize train and test separately
    [meanv, stdv, train(:, 1 : n_features)] = normalize(train(:, 1 : n_features), [], []);
    [meanv, stdv, test(:, 1 : n_features)] = normalize(test(:, 1 : n_features), meanv, stdv);
    
    % add additional column of ones so that LR can be done
    train = [train(:, 1 : n_features) ones(size(train, 1), 1) train(:, n_features + 1)];
    test = [test(:, 1 : n_features) ones(size(test, 1), 1) test(:, n_features + 1)];
    
    % train predictor
    results = logit(train(:, size(train, 2)), train(:, 1 : n_features + 1));
    
    % get prediction on test data
    prediction = test(:, 1 : n_features + 1) * results.beta;
    % transform results to 0-1 interval (logsig function)
    prediction = 1 ./ (1 + exp(-prediction));
    
    q = find(prediction >= 0.5);
    x(q) = 1;
    q = find(prediction < 0.5);
    x(q) = 0;
    
    pred_error = x' - test(:, n_features + 2);

    q00 = find(pred_error == 0 & test(:, n_features + 2) == 0); 
    q01 = find(pred_error ~= 0 & test(:, n_features + 2) == 1); % predicted 0, true 1
    q10 = find(pred_error ~= 0 & test(:, n_features + 2) == 0);
    q11 = find(pred_error == 0 & test(:, n_features + 2) == 1);
    
    sn(i) = length(q11) / (length(q01) + length(q11));
    sp(i) = length(q00) / (length(q10) + length(q00));
end
    
sensitivity = mean(sn);
specificity = mean(sp);
sensitivity_std = std(sn);
specificity_std = std(sp);

return