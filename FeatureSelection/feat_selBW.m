function [best_features, removed_features, sensitivity, sensitivity_std, specificity, specificity_std] = feat_selBW (sample, Eps)

%
% function that selects best set of features removing one at a time from a complete set
% INPUT: sample, usual data matrix, last column is binary target
%        Eps, stopping criterion for feature selection (usually small positive value, say 1e-4)
% OUTPUT: best_features, set of selected features
%         removed_features, set of removed features in each iteration (in the first iteration there are
%                           no removed features (NaN value) e.g. removed_features(2) is a feature removed
%                           in the second iteration (of course, features once removed stay removed)
%         sensitivity, estimated sensitivity of prediction for each selected feature
%         specificity, estimated specificity of prediction for each selected feature
%         sensitivity_std, standard deviation of sensitivity for each selected feature
%         specificity_std, standard deviation of specificity for each selected feature
%

% initialize things
sensitivity = [];
sensitivity_std = [];
specificity = [];
specificity_std = [];
accuracy = zeros(1, size(sample, 2) - 1);

% start from the set of all features and calculate accuracy
best_features = [1 : size(sample, 2) - 1];
removed_features = [NaN];
[sensitivity(1) sensitivity_std(1) specificity(1) specificity_std(1)] = calculate_prediction_accuracy(sample, best_features);
accuracy(1) =  0.5 * (sensitivity(1) + specificity(1));

iteration = 2;
accuracy_last_round = accuracy(1);
eps = accuracy(1);

% until adding features improves accuracy
while eps > Eps & ~isempty(best_features)
    fprintf(1, '\n\nCurrent accuracy: %.2f', 100 * accuracy_last_round);
    fprintf(1, '\Removing features...');
    
    tsn = []; tsp = [];
    for i = 1 : length(best_features)
        fprintf(1, '\n    iteration %d/%d removing feature %d', i, length(best_features), best_features(i));

        % remove feature i
        features = setdiff(best_features, best_features(i));

        % caclulate sensitivity and specificity
        [tsn(i) tsn_std(i) tsp(i) tsp_std(i)] = calculate_prediction_accuracy(sample, features);

        % calculate the accuracy of the i-th added feature
        acc(i) = 0.5 * (tsn(i) + tsp(i));
        fprintf(1, ' (%.2f)', 100* acc(i));
    end

    % find best removed feature and overall accuracy
    [accuracy_this_round pos] = max(acc);

    % is there any improvement from the last step, i.e. does the removed feature help
    eps = accuracy_this_round - accuracy_last_round;

    % we do not allow removing features that decrease accuracy
    if eps > Eps
        % remove best feature from the vector of selected features
        best_features = setdiff(best_features, best_features(pos));
        % add feature to removed features
        removed_features = [removed_features best_features(pos)];

        % store estimates for sensitivity and specificity and their std's
        sensitivity(iteration) = tsn(pos);
        sensitivity_std(iteration) = tsn_std(pos);
        specificity(iteration) =  tsp(pos);
        specificity_std(iteration) = tsp_std(pos);

        % caclulate accuracy
        accuracy(iteration) =  0.5 * (sensitivity(iteration) + specificity(iteration));
        
        % store epsilon (in case anybody needs it)
        epsilon(iteration) = eps;

        iteration = iteration + 1;
    end
    
    accuracy_last_round = accuracy_this_round;
end

return