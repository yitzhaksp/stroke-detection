%{
given a set of features and a subset of selected features
this function computes the relevance and the redundancy
of each of the nonselected features.

min Redundancy Max Relevance method for feature selection
B. Bonev - FEATURE SELECTION
BASED ON INFORMATION THEORY (Chapter 2.4.5, eq 2.23)

Input:
Feat: (N,M)  featurematrix where N=numsamp, M=numfeat
Classes: N-vector of Labels
FeatSelInd: Indices of already selected features

variables:
RedRel: selection criterion: RedRel(feature)=Relevance(feature)-Redundancy(feature)
output: index of next selected feature 
%}
function [Rel Red]=CompRelRed(Feat,Classes,FeatSelInd)
M=size(Feat,2); %num features
Msel=length(FeatSelInd); %num of already selected features
Mnotsel=M-Msel;
FeatNotselInd=ComputeIndCompl(M,FeatSelInd);
Red=zeros(1,Mnotsel); 
Rel=zeros(1,Mnotsel); 
for j=1:Mnotsel
    Red(j)=0; %redundancy
    for i=1:Msel
        indj=FeatNotselInd(j);
        indi=FeatSelInd(i);
        Feat1=Feat(:, FeatNotselInd(j));
        Feat2=Feat(:, FeatSelInd(i));
        mi=mutualinfo_mtlb(Feat1,  Feat2);
        Red(j)=Red(j)+mi;
    end
    Rel(j)=mutualinfo_mtlb(  Feat(:, FeatNotselInd(j))  ,  Classes  );
end
