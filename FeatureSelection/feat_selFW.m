function [best_features, sensitivity, sensitivity_std, specificity, specificity_std] = feat_selFW (sample, Eps)

%
% function that selects best set of features adding one at a time
% INPUT: sample, usual data matrix, last column is binary target
%        Eps, stopping criterion for feature selection (usually small positive value, say 1e-4)
% OUTPUT: best_features, set of selected features
%         sensitivity, estimated sensitivity of prediction for each selected feature
%         specificity, estimated specificity of prediction for each selected feature
%         sensitivity_std, standard deviation of sensitivity for each selected feature
%         specificity_std, standard deviation of specificity for each selected feature
%

% this set will get some features taken out
remaining_features = [1 : size(sample, 2) - 1];

% this set will contain the "optimal" features
best_features = []; 

% initialize
eps = 100; % difference in accuracy between this and last round
accuracy_last_round = 0; % accuracy in previous round

% initialize things
iteration = 1;
sensitivity = [];
sensitivity_std = [];
specificity = [];
specificity_std = [];
accuracy = [];

% ---------------------------------------------------------------------------------------
% Feature selection starts here
% ---------------------------------------------------------------------------------------

% until adding features improves accuracy and there are features to add
while eps > Eps & ~isempty(remaining_features)
    fprintf(1, '\n\nCurrent accuracy: %.2f', 100 * accuracy_last_round);
    fprintf(1, '\nAdding features...');
    
    acc = zeros(1, length(remaining_features));

    tsn = []; tsp = [];
    for i = 1 : length(remaining_features)
        fprintf(1, '\n    iteration %d/%d using feature %d', i, length(remaining_features), remaining_features(i));

        % add feature i
        features = [best_features remaining_features(i)];

        % caclulate sensitivity and specificity
        [tsn(i) tsn_std(i) tsp(i) tsp_std(i)] = calculate_prediction_accuracy(sample, features);

        % calculate the accuracy of the i-th added feature
        acc(i) = 0.5 * (tsn(i) + tsp(i));
        fprintf(1, ' (%.2f)', 100* acc(i));
    end

    % find best new feature and overall accuracy
    [accuracy_this_round pos] = max(acc);

    % is there any improvement from the last step, i.e. does the additional feature help
    eps = accuracy_this_round - accuracy_last_round;

    % we do not allow taking features that decrease accuracy
    if eps > Eps
        % add best feature to the vector of selected features
        best_features = [best_features remaining_features(pos)];

        % take out just selected feature from the remaining feature list
        remaining_features = ...
            [remaining_features(1 : pos - 1) remaining_features(pos + 1 : length(remaining_features))];

        % store estimates for sensitivity and specificity and their std's
        sensitivity(iteration) = tsn(pos);
        sensitivity_std(iteration) = tsn_std(pos);
        specificity(iteration) =  tsp(pos);
        specificity_std(iteration) = tsp_std(pos);

        % caclulate accuracy
        accuracy(iteration) =  0.5 * (sensitivity(iteration) + specificity(iteration));
        
        % store epsilon (in case anybody needs it)
        epsilon(iteration) = eps;

        iteration = iteration + 1;
    end
    
    accuracy_last_round = accuracy_this_round;
end

return