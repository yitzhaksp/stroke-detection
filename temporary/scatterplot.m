addpath(genpath('../.'));
clear all
nFeatAct=20; % num of active features    
    LoadFeatRank=1; % load featurerank
    MLFeatFlag=1; % calc additional features via machine learning
    set_loadparams;
    [Feat,Cl,ClProb,NCl,IndTrain,IndConf, ...
     IndTest,DataStat]=Load_and_Separate(DataFlag,opts);
    if LoadFeatRank
        switch DataFlag
            case 1
                load('FeatRank_aug14');
            case 2
                load('FeatRank_jul15');
        end
    else
        FeatSelMeth=2; %1:shannon, 2:mrmr
        FeatRank=Feature_Selection(Feat(:,IndTrain)',Cl(IndTrain),FeatSelMeth);
    end
    FeatAct=FeatRank(1:nFeatAct);
    Feat=Feat(FeatAct,:);
    if MLFeatFlag
         ntrain=length(IndTrain);
        trainAFratio=.5;
        ntrainAF=floor(ntrain/2);
        IndTrainAF=IndTrain(1:ntrainAF); % training set for computing processed features
        IndTrain=IndTrain((ntrainAF+1):end); % training set for top level classifier
        %which processed features shall be computed? 
        params.svm=1; 
        params.knn=1;        
        FeatML=calc_artificial_features(Feat,Cl,IndTrainAF,params);
        Feat=FeatML;
    end
    
    indpos=(Cl==1);
    plot(Feat(1,indpos),Feat(2,indpos),'r*');
    hold on;
    indneg=(Cl==0);
    plot(Feat(1,indneg),Feat(2,indneg),'b*');

