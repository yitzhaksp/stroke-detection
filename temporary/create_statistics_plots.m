load('stats_Jul15_nn')
nn_stats_15=[stats.acr_ind,stats(1).PPV_ind,stats(1).NPV_ind(1)];
load('stats_Jul15_nearneig')
nearneig_stats_15=[stats.acr_ind(1),stats.PPV_ind(1),stats.NPV_ind(1)];
load('stats_Jul15_svm')
svm_stats_15=[stats.acr_ind(1),stats.PPV_ind(1),stats.NPV_ind(1)];
load('stats_aug14_nn')
nn_stats_14=[stats.acr_ind,stats(1).PPV_ind,stats(1).NPV_ind(1)];
load('stats_aug14_nearneig')
nearneig_stats_14=[stats.acr_ind(1),stats.PPV_ind(1),stats.NPV_ind(1)];
load('stats_aug14_svm')
svm_stats_14=[stats.acr_ind(1),stats.PPV_ind(1),stats.NPV_ind(1)];
subplot(3,2,1), grid on, bar(nn_stats_14); title('nn 14');grid on;
subplot(3,2,2), grid on, bar(nn_stats_15); title('nn 15');grid on;
subplot(3,2,3), grid on, bar(svm_stats_14); title('svm 14');grid on;
subplot(3,2,4), grid on, bar(svm_stats_15); title('svm 15');grid on;
subplot(3,2,5), grid on, bar(nearneig_stats_14); title('nearneig 14');grid on;
subplot(3,2,6), grid on, bar(nearneig_stats_15); title('nearneig 15');grid on;