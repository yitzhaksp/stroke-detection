addpath(genpath('.'));
addpath('../useful_functions');
load('partitions'); 
opts=set_loadparams();
opts.nFeatAct=45;
partition_in=partition(1);
[Feat,Cl,ClProb,DataStat,partition_out]=Load_data(partition_in,opts);
opts1.nNeig=80;
opts1.trh_Flag=0; % use treshold? 0:no, 1: opts.trh_cls, 2:calibrate trh
opts1.trh_cls=.4;
opts1.print=1;
[Prd,Scores]=Classification_KNearNeig_fun(Feat,Cl,partition_out,opts1);
%compute statistics
arg1.name='trh_FP';
arg1.value=.1;
arg1.scores=Scores;
stats=comp_stat(Prd,Cl(partition_out.IndTest),arg1);
opts2.A_trh=1;
print_statistics(stats,opts2);
[X3,Y3]=perfcurve(Cl(partition_out.IndTest),Scores,1);
