function [nstates,desdata]=copyvecdata_mtlb(srcdata)
ndata=length(srcdata);
for i=1:ndata
    tmp1=srcdata(i);
    if (tmp1>0)
        tmp=ceil(tmp1+0.5);
    else
        tmp=ceil(tmp1-0.5);
    end
    desdata(i)=tmp;
end
minn=min(desdata);
maxx=max(desdata);
desdata = desdata-minn;
nstates = maxx-minn+1;




