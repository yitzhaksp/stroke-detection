function MI = mutualinfo(vec1,vec2)
%=========================================================
%
%This is a prog in the MutualInfo 0.9 package written by 
% Hanchuan Peng.
%
%Disclaimer: The author of program is Hanchuan Peng
%      at <penghanchuan@yahoo.com> and <phc@cbmv.jhu.edu>.
%
%The CopyRight is reserved by the author.
%
%Last modification: April/19/2002
%
%========================================================
%
% h = mutualinfo(vec1,vec2)
% calculate the mutual information of two vectors
% By Hanchuan Peng, April/2002
%

[p12, p1, p2] = estpab_ys(vec1,vec2);
%[py12, py1, py2] = estpab_mtlb(vec1,vec2);
%diff1=p1-py1'; 
%diff2=p2-py2'; 
%diff12=p12-py12';
MI = estmutualinfo(p12,p1,p2);
%MI1 = estmutualinfo_mtlb(py12,py1,py2);
%diffmi=MI1-MI


