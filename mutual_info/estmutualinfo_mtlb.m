function muInf=estmutualinfo_mtlb(pab,pa,pb)
pabhei = size(pab,1);
pabwid = size(pab,2);
muInf = 0.0;
for i=1:pabwid
    for j=1:pabhei
        if (pab(j,i)~=0 && pa(i)~=0 && pb(j)~=0)
            muInf = muInf + pab(j,i)*log(pab(j,i)/(pa(i)*pb(j)));
        end
    end
end

muInf = muInf/log(2.0);