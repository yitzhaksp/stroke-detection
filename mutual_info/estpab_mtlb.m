function [pab2d, pa, pb]=estpab_mtlb(a,b)
na=length(a); nb=length(b);
nstate1 = 0; nstate2 = 0;
b_returnprob = 1;
[nrealstate1,vec1]=copyvecdata_mtlb(a);
[nrealstate2,vec2]=copyvecdata_mtlb(b);
  if (nstate1<nrealstate1)
    nstate1 = nrealstate1;
  end
  if (nstate2<nrealstate2)
    nstate2 = nrealstate2;
  end
  pab2d=zeros(nstate2,nstate1);
  for i=1:na
    pab2d(vec2(i)+1,vec1(i)+1) = pab2d(vec2(i)+1,vec1(i)+1)+ 1;
  end
  if(b_returnprob)
      pab2d=pab2d/na;
  end
  for j=1:nstate1
      pa(j)=sum(pab2d(:,j));
  end
  for i=1:nstate2
      pb(i)=sum(pab2d(i,:));
  end;